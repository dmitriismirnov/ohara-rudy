#!/bin/bash

for f in *.csv ohara_rudy CMakeCache.txt cmake_install.cmake Makefile
do
    if [[ -f "$f" ]]
    then
        rm $f
        echo -e "\033[31;1m$0: $f is deleted\033[0m"
    else
        echo "$0: $f does not exist"
    fi
done

for d in CMakeFiles cmake-build-debug .idea
do
    if [[ -d "$d" ]]
    then
        rm $d -r
        echo -e "\033[31;1m$0: $d is deleted\033[0m"
    else
        echo "$0: $d does not exist"
    fi
done	

echo "directory is clear"

