//
// Created by andrey on 0.4/12/18.
//

#ifndef OHARA_RUDY_VARIABLES_H
#define OHARA_RUDY_VARIABLES_H

struct Variables {

    Variables() : ENa(0.), EK(0.), EKs(0.),
                  dss(0.), fss(0.),
                  INa(0.), INaL(0.), Ito(0.), ICaL(0.), ICaNa(0.), ICaK(0.), IKr(0.), IKs(0.), IK1(0.), INaCa_i(0.), INaCa_ss(0.),
                  INaCa(0.), INaK(0.), IKb(0.), INab(0.), IpCa(0.), ICab(0.), Ist(0.), IKatp(0.), I_gap_junc(0.),
                  Jrel(0.), Jup(0.), Jtr(0.), Jdiff(0.), JdiffNa(0.), JdiffK(0.), Jleak(0.),
                  CaMKa(0.), CaMKb(0.) {}

    double ENa, EK, EKs;
    double dss, fss;
    double INa, INaL, Ito, ICaL, ICaNa, ICaK, IKr, IKs, IK1, INaCa_i, INaCa_ss, INaCa, INaK, IKb, INab, IpCa, ICab, Ist, IKatp;
    double I_gap_junc;
    double Jrel, Jup, Jtr, Jdiff, JdiffNa, JdiffK, Jleak;
    double CaMKa, CaMKb;

};

#endif //OHARA_RUDY_VARIABLES_H
