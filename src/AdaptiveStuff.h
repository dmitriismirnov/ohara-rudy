//
// Created by andrey on 04/12/18.
//

#ifndef OHARA_RUDY_ADAPTIVESTUFF_H
#define OHARA_RUDY_ADAPTIVESTUFF_H

#include <cmath>
#include "State.h"

double get_dv_max(State *CurrentState, State *NextState, int chain_length);

#endif //OHARA_RUDY_ADAPTIVESTUFF_H
