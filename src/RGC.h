//
// Created by andrey on 03/12/18.
//

#ifndef OHARA_RUDY_RGC_H
#define OHARA_RUDY_RGC_H

#include <cmath>
#include <cstdio>
#include <iostream>
#include "State.h"
#include "Constants.h"
#include "Variables.h"
#include "Parameters.h"
#include "Tables.h"

#include "currents/INa.h"
#include "currents/INaL.h"
#include "currents/ICaL_ICaN_ICaK.h"
#include "currents/Ito.h"
#include "currents/IKr.h"
#include "currents/IKs.h"
#include "currents/IK1.h"
#include "currents/INaCa.h"
#include "currents/INaK.h"
#include "currents/INab_ICab_IpCa.h"
#include "currents/IKATP.h"
#include "currents/Jrel.h"
#include "currents/Jup.h"

void RGC(State *CurrentState, State *NextState,
         Variables *Var, Parameters *Par, Constants *Cnst,
         Table *Tbl = nullptr);

#endif //OHARA_RUDY_RGC_H
