#ifndef OHARA_RUDY_CONFIG_FILE_H
#define OHARA_RUDY_CONFIG_FILE_H

#include <string>
#include <map>

#include "Chameleon.h"

#define DEFINE_FROM_CONFIGFILE(type, config_file, section, entry) \
type entry = (config_file).Value(#section, #entry); //std::cout << #section << ": " << #entry << " = " << entry << "\n";

class ConfigFile {

  std::map<std::string,Chameleon> content_;

public:

  ConfigFile(std::string const& configFile);

  Chameleon const& Value(std::string const& section, std::string const& entry) const;

  Chameleon const& Value(std::string const& section, std::string const& entry, double value);

  Chameleon const& Value(std::string const& section, std::string const& entry, std::string const& value);
};

#endif
