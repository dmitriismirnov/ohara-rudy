//
// Created by andrey on 04/12/18.
//

#include "AdaptiveStuff.h"

double get_dv_max(State *CurrentState, State *NextState, int chain_length) {
    double dv_max = fabs((NextState[0].v - CurrentState[0].v));
    for (int i = 1; i < chain_length; i++) {
        double dv = fabs((NextState[i].v - CurrentState[i].v));
        if (dv > dv_max) {
            dv_max = dv;
        }
    }
    return dv_max;
}
