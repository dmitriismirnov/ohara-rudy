//
// Created by andrey on 02/12/18.
//

#ifndef OHARA_RUDY_CONSTANTS_H
#define OHARA_RUDY_CONSTANTS_H

//physical constants
constexpr double R = 8314.0;
constexpr double T = 310.0;
constexpr double F = 96485.0;
constexpr double FRT = F / (R * T);
constexpr double FFRT = (F * F) / (R * T);
constexpr double RTF = (R * T) / F;

struct Constants {

    Constants(double const &c_gkatp = 1.,
              double const &c_jrelinf = 1.,
              double const &c_gk1 = 1.,
              double const &c_gkr = 1.,
              double const &c_gks = 1.,
              double const &c_gnal = 1.,
              double const &c_gto = 1.,
              double const &c_pca = 1.,
              double const &c_gncx = 1.,
              double const &c_pnak = 1.,
              double const &c_gpca = 1.,
              double const &c_jup = 1.,
              double const &c_CMDN = 1.,
              double const &c_arel = 1.,
              double const &c_CaMKo = 1.,
              double const &c_gkb = 1.,
              double const &c_gcab = 1.,
              double const &c_gnab = 1.,

              ///iso
              const bool &ISO = false,
              double const &c_INa_dV_act = 1.046,
              double const &c_INa_dV_inact = 0.543,
              double const &c_ICaL_dV_act = 0.959,
              double const &c_ICaL_dV_inact = 1.228,
              double const &c_Knai = 1.026,
              double const &c_GKb_iso = 0.617,
              double const &c_SERCA = 0.995,
              double const &c_GNa_ISO = 1.012,
              double const &c_Tnl = 1.155,
              double const &c_IKs_ISO = 0.958,
              double const &c_txs_1_iso = 0.459,
              double const &c_a_rel = 1.051,
              double const &c_tau_rel = 0.993,

              ///concentrations
              double const &nao = 140.0,//extracellular sodium in mM
              double const &cao = 1.8,//extracellular calcium in mM
              double const &ko = 5.4,//extracellular potassium in mM

              ///cell properties
              int const &celltype = 0,

              ///gap
              double const &g_gap_junc = 5.0,

              ///ikatp
              double const &f_atp = 0,

              bool IKR_GRANDI = false)

            : c_gkatp(c_gkatp), c_jrelinf(c_jrelinf), c_gk1(c_gk1), c_gkr(c_gkr), c_gks(c_gks), c_gnal(c_gnal),
              c_gto(c_gto), c_pca(c_pca), c_gncx(c_gncx), c_pnak(c_pnak), c_gpca(c_gpca), c_jup(c_jup), c_CMDN(c_CMDN),
              c_arel(c_arel), c_CaMKo(c_CaMKo), c_gkb(c_gkb), c_gcab(c_gcab), c_gnab(c_gnab),

              ISO(ISO),
              c_INa_dV_act(c_INa_dV_act), c_INa_dV_inact(c_INa_dV_inact), c_ICaL_dV_act(c_ICaL_dV_act),
              c_ICaL_dV_inact(c_ICaL_dV_inact), c_Knai(c_Knai), c_GKb_iso(c_GKb_iso), c_SERCA(c_SERCA),
              c_GNa_ISO(c_GNa_ISO), c_Tnl(c_Tnl), c_IKs_ISO(c_IKs_ISO), c_txs_1_iso(c_txs_1_iso),
              c_a_rel(c_a_rel), c_tau_rel(c_tau_rel),

              nao(nao), cao(cao), ko(ko),

              BSRmax(0.047), KmBSR(0.00087), BSLmax(1.124), KmBSL(0.0087), cmdnmax(0.05), kmcmdn(0.00238),
              trpnmax(0.07), kmtrpn(0.0005), csqnmax(10.0), kmcsqn(0.8),

              aCaMK(0.05), bCaMK(0.00068), CaMKo(0.05), KmCaM(0.0015), KmCaMK(0.15),

              celltype(celltype),

              L(0.01), rad(0.0011), vcell(1000 * 3.14 * rad * rad * L), Ageo(2 * 3.14 * rad * rad + 2 * 3.14 * rad * L),
              Acap(2 * Ageo), vmyo(0.68 * vcell),
              vmito(0.26 * vcell), vsr(0.06 * vcell), vnsr(0.0552 * vcell), vjsr(0.0048 * vcell), vss(0.02 * vcell),

              g_gap_junc(g_gap_junc),

              f_atp(f_atp),

              IKR_GRANDI(IKR_GRANDI){} // end of constructor

    double const c_gkatp;
    double const c_jrelinf;
    double const c_gk1;
    double const c_gkr;
    double const c_gks;
    double const c_gnal;
    double const c_gto;
    double const c_pca;
    double const c_gncx;
    double const c_pnak;
    double const c_gpca;
    double const c_jup;
    double const c_CMDN;
    double const c_arel;
    double const c_CaMKo;
    double const c_gkb;
    double const c_gcab;
    double const c_gnab;

    //iso coefficients
    const bool ISO;
    double const c_INa_dV_act;
    double const c_INa_dV_inact;
    double const c_ICaL_dV_act;
    double const c_ICaL_dV_inact;
    double const c_Knai;
    double const c_GKb_iso;
    double const c_SERCA;
    double const c_GNa_ISO;
    double const c_Tnl;
    double const c_IKs_ISO;
    double const c_txs_1_iso;
    double const c_a_rel;
    double const c_tau_rel;

    //constants
    double const nao;//extracellular sodium in mM
    double const cao;//extracellular calcium in mM
    double const ko;//extracellular potassium in mM

    //buffer paramaters
    double const BSRmax;
    double const KmBSR;
    double const BSLmax;
    double const KmBSL;
    double const cmdnmax;
    double const kmcmdn;
    double const trpnmax;
    double const kmtrpn;
    double const csqnmax;
    double const kmcsqn;

    //CaMK paramaters
    double const aCaMK;
    double const bCaMK;
    double const CaMKo;
    double const KmCaM;
    double const KmCaMK;

    //cell properties
    const int celltype;  //endo = 0, epi = 1, M = 2

    double const L;
    double const rad;
    double const vcell;
    double const Ageo;
    double const Acap;
    double const vmyo;
    double const vmito;
    double const vsr;
    double const vnsr;
    double const vjsr;
    double const vss;

    double const g_gap_junc;

    double const f_atp;

    bool IKR_GRANDI;
};


#endif //OHARA_RUDY_CONSTANTS_H
