#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <string>
#include <iostream>
#include <fstream>

#include "Init.h"
#include "Constants.h"
#include "FileRoutine.h"
#include "Variables.h"
#include "Parameters.h"
#include "RGC.h"
#include "AdaptiveStuff.h"
#include "Tables.h"
#include "ConsoleOutput.h"

int main(int argc, char *argv[]) {

    clock_t time_start, time_end;
    time_start = clock();

    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " config_filename\n";
        std::cout << "Example: " << argv[0] << " config_default.txt\n";
        return 1;
    }

    auto config_filename = (std::string) argv[1];
    ConfigFile cf(config_filename);

    bool ADAPTIVE_STEP, READIS, WRITEIS, TABLE, CONSOLE_OUTPUT;
    std::string state_initial_filename, state_final_filename, csv_filename;
    Parameters *Par = nullptr;
    Constants *Cnst = nullptr;
    Table *Tbl = nullptr;
    int target_cell_index;

    try {

        ADAPTIVE_STEP = (bool) cf.Value("flags", "ADAPTIVE_STEP");

        READIS = (bool) cf.Value("states", "READIS");
        WRITEIS = (bool) cf.Value("states", "WRITEIS");

        if (READIS) {
            state_initial_filename = "./states/" + (std::string) cf.Value("states", "file_to_read");
        }
        if (WRITEIS) {
            state_final_filename = "./states/" + (std::string) cf.Value("states", "file_to_write");
        }

        csv_filename = (std::string) cf.Value("output", "file_csv");

        TABLE = (bool) cf.Value("flags", "TABLE");

        Par = load_parameters_from_file(config_filename);

        std::string cell_type_filename =
                "./cell_types/" + (std::string) cf.Value("cell_properties", "cell_type_filename");
        Cnst = load_constants_from_file(cell_type_filename);

        if (TABLE) {
            double v_min = cf.Value("flags", "v_min");
            double v_max = cf.Value("flags", "v_max");
            int num = cf.Value("flags", "num");
            Tbl = new Table(v_min, v_max, num, Cnst);
        }

        target_cell_index = (int) cf.Value("output", "target_cell_index");

        if (target_cell_index >= Par->chain_length) {
            std::string report = "Target cell is outside of the chain";
            throw std::runtime_error(report);
        }

        CONSOLE_OUTPUT = (bool) cf.Value("output", "CONSOLE_OUTPUT");

    } catch (const std::exception &e) {
        std::cerr << e.what() << "\n";
        return 1;
    }

    auto *Var = new Variables[Par->chain_length];

    auto *CurrentState = new State[Par->chain_length];
    auto *NextState = new State[Par->chain_length];

    for (int i = 0; i < Par->chain_length; i++) {
        if (READIS) {
            try {
                update_state_from_file(&CurrentState[i], state_initial_filename);
            } catch (const std::exception &e) {
                std::cerr << e.what() << "\n";
                return 1;
            }
        } else {
            initialize_state(&CurrentState[i]);
        }
        //CurrentState[i] = NextState[i];
    }

    std::ofstream file_csv;
    file_csv.open(csv_filename);
    create_header_csv(file_csv);
    /*
    auto *FilesToWrite = new Files;
    if (Cnst->ISO == true) {
        open_iso(FilesToWrite);
    } else {
        open(FilesToWrite);
    }
     */

    double dv = 0.;
    double t_output = 0.;

    auto CPrinter = ConsolePrinter();

    /// MAIN CYCLE ////////
    while (Par->t <= Par->ft) {

        if (ADAPTIVE_STEP) {

            if (((Par->t >= (Par->t_stimulus - 2)) &&
                 (Par->t < (Par->t_stimulus + Par->duration))) ||
                (Par->t < (Par->t_stimulus - Par->CL + Par->duration + Par->safe_time))) {
                Par->dt = 0.005;
            } else if (dv > 0.08) {
                Par->t = Par->t - Par->dt;
                Par->dt = fabs(0.02 * Par->dt / dv);
                RGC(CurrentState, NextState, Var, Par, Cnst);
                Par->t = Par->t + Par->dt;
                while ((get_dv_max(CurrentState, NextState, Par->chain_length) > 0.08) && (Par->dt > 0.005)) {
                    Par->t = Par->t - Par->dt;
                    Par->dt = Par->dt / 2.0;
                    if (Par->dt < 0.005) {
                        Par->dt = 0.005;
                    }
                    RGC(CurrentState, NextState, Var, Par, Cnst);
                    Par->t = Par->t + Par->dt;
                }
            } else if (dv < 0.02) {
                Par->dt = fabs(0.08 * Par->dt / dv);
                if (Par->dt > 0.09) {
                    Par->dt = 0.09;
                }
            }

            dv = get_dv_max(CurrentState, NextState, Par->chain_length);
        } // if (ADAPTIVE_STEP == true)


        RGC(CurrentState, NextState, Var, Par, Cnst, Tbl);

        for (int i = 0; i < Par->chain_length; i++) {
            CurrentState[i] = NextState[i];
        }

        Par->t = Par->t + Par->dt;

        if ((ADAPTIVE_STEP && (Par->t >= t_output)) ||
            (!ADAPTIVE_STEP && (Par->count % Par->skip == 0))) {

            if (Par->beats_count > Par->number_of_beats - Par->beats_save) {

                //print_to_files(FilesToWrite, &CurrentState[target_cell_index], &Var[target_cell_index], Par, Par->chain_length);
                print_to_scv(&CurrentState[target_cell_index], &Var[target_cell_index], Par, file_csv);

                if (t_output == 0) { //first print moment
                    t_output = Par->t;
                }
                t_output += Par->output_step;
            }

            if (CONSOLE_OUTPUT) {
                CPrinter.print(Par);
            }
        }

        Par->count++;
    }

    CPrinter.skip_filled_lines();

    if (WRITEIS) {

        FILE *state_final = fopen(state_final_filename.c_str(), "w");

        if (!state_final) {
            std::string report = "Can't open \"" + state_final_filename + "\" for state saving";
            std::cerr << report << "\n";
        } else {
            fwrite(&CurrentState[target_cell_index], sizeof(State), 1, state_final);
        }
        fclose(state_final);
    }


    //close(FilesToWrite);
    //delete FilesToWrite;
    file_csv.close();

    delete Par;
    delete Cnst;
    delete[] Var;
    delete[] CurrentState;
    delete[] NextState;
    delete Tbl;

    time_end = clock();
    std::cout << "CPU_time: " << (float) (time_end - time_start) / (CLOCKS_PER_SEC) / 60. << " (min)\n";
    return 0;
}
