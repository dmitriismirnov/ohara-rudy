//
// Created by andrey on 03/12/18.
//

#include "FileRoutine.h"

void open(Files *F) {

    F->t_ = fopen("./output/t.txt", "w");
    F->v_ = fopen("./output/v.txt", "w");
    F->nai_ = fopen("./output/nai.txt", "w");
    F->nass_ = fopen("./output/nass.txt", "w");
    F->ki_ = fopen("./output/ki.txt", "w");
    F->kss_ = fopen("./output/kss.txt", "w");
    F->cai_ = fopen("./output/cai.txt", "w");
    F->cass_ = fopen("./output/cass.txt", "w");
    F->cansr_ = fopen("./output/cansr.txt", "w");
    F->cajsr_ = fopen("./output/cajsr.txt", "w");
    F->Jrel_ = fopen("./output/Jrel.txt", "w");
    F->CaMKt_ = fopen("./output/CaMKt.txt", "w");
    F->Jup_ = fopen("./output/Jup.txt", "w");
    F->Jtr_ = fopen("./output/Jtr.txt", "w");
    F->Jdiff_ = fopen("./output/Jdiff.txt", "w");
    F->JdiffNa_ = fopen("./output/JdiffNa.txt", "w");
    F->JdiffK_ = fopen("./output/JdiffK.txt", "w");
    F->Jleak_ = fopen("./output/Jleak.txt", "w");
    F->INa_ = fopen("./output/INa.txt", "w");
    F->INaL_ = fopen("./output/INaL.txt", "w");
    F->Ito_ = fopen("./output/Ito.txt", "w");
    F->ICaL_ = fopen("./output/ICaL.txt", "w");
    F->ICaNa_ = fopen("./output/ICaNa.txt", "w");
    F->ICaK_ = fopen("./output/ICaK.txt", "w");
    F->IKr_ = fopen("./output/IKr.txt", "w");
    F->IKs_ = fopen("./output/IKs.txt", "w");
    F->IK1_ = fopen("./output/IK1.txt", "w");
    F->INaCa_i_ = fopen("./output/INaCa_i.txt", "w");
    F->INaCa_ss_ = fopen("./output/INaCa_ss.txt", "w");
    F->INaCa_ = fopen("./output/INaCa.txt", "w");
    F->INaK_ = fopen("./output/INaK.txt", "w");
    F->IKb_ = fopen("./output/IKb.txt", "w");
    F->INab_ = fopen("./output/INab.txt", "w");
    F->IpCa_ = fopen("./output/IpCa.txt", "w");
    F->IKatp_ = fopen("./output/IKatp.txt", "w");
    F->ICab_ = fopen("./output/ICab.txt", "w");
    F->Ist_ = fopen("./output/Ist.txt", "w");
    F->dt_ = fopen("./output/dt.txt", "w");
    F->ENa_ = fopen("./output/ENa.txt", "w");
    F->EK_ = fopen("./output/EK.txt", "w");
    F->EKs_ = fopen("./output/EKs.txt", "w");

// for ICaL
    F->f_ = fopen("./output/f.txt", "w");
    F->d_ = fopen("./output/d.txt", "w");
    F->nca_ = fopen("./output/nca.txt", "w");
    F->fca_ = fopen("./output/fca.txt", "w");
    F->jca_ = fopen("./output/jca.txt", "w");
    F->fp_ = fopen("./output/fp.txt", "w");
    F->fcap_ = fopen("./output/fcap.txt", "w");
    F->tff_ = fopen("./output/tff.txt", "w");
    F->tfs_ = fopen("./output/tfs.txt", "w");
    F->tffp_ = fopen("./output/tffp.txt", "w");
    F->dss_ = fopen("./output/dss.txt", "w");
    F->fss_ = fopen("./output/fss.txt", "w");
    F->Ical_V = fopen("./output/ICaL_V.txt", "w");
//
}

void open_iso(Files *F) {
    F->t_ = fopen("./output_iso/t.txt", "w");
    F->v_ = fopen("./output_iso/v.txt", "w");
    F->nai_ = fopen("./output_iso/nai.txt", "w");
    F->nass_ = fopen("./output_iso/nass.txt", "w");
    F->ki_ = fopen("./output_iso/ki.txt", "w");
    F->kss_ = fopen("./output_iso/kss.txt", "w");
    F->cai_ = fopen("./output_iso/cai.txt", "w");
    F->cass_ = fopen("./output_iso/cass.txt", "w");
    F->cansr_ = fopen("./output_iso/cansr.txt", "w");
    F->cajsr_ = fopen("./output_iso/cajsr.txt", "w");
    F->Jrel_ = fopen("./output_iso/Jrel.txt", "w");
    F->CaMKt_ = fopen("./output_iso/CaMKt.txt", "w");
    F->Jup_ = fopen("./output_iso/Jup.txt", "w");
    F->Jtr_ = fopen("./output_iso/Jtr.txt", "w");
    F->Jdiff_ = fopen("./output_iso/Jdiff.txt", "w");
    F->JdiffNa_ = fopen("./output_iso/JdiffNa.txt", "w");
    F->JdiffK_ = fopen("./output_iso/JdiffK.txt", "w");
    F->Jleak_ = fopen("./output_iso/Jleak.txt", "w");
    F->INa_ = fopen("./output_iso/INa.txt", "w");
    F->INaL_ = fopen("./output_iso/INaL.txt", "w");
    F->Ito_ = fopen("./output_iso/Ito.txt", "w");
    F->ICaL_ = fopen("./output_iso/ICaL.txt", "w");
    F->ICaNa_ = fopen("./output_iso/ICaNa.txt", "w");
    F->ICaK_ = fopen("./output_iso/ICaK.txt", "w");
    F->IKr_ = fopen("./output_iso/IKr.txt", "w");
    F->IKs_ = fopen("./output_iso/IKs.txt", "w");
    F->IK1_ = fopen("./output_iso/IK1.txt", "w");
    F->INaCa_i_ = fopen("./output_iso/INaCa_i.txt", "w");
    F->INaCa_ss_ = fopen("./output_iso/INaCa_ss.txt", "w");
    F->INaCa_ = fopen("./output_iso/INaCa.txt", "w");
    F->INaK_ = fopen("./output_iso/INaK.txt", "w");
    F->IKb_ = fopen("./output_iso/IKb.txt", "w");
    F->INab_ = fopen("./output_iso/INab.txt", "w");
    F->IpCa_ = fopen("./output_iso/IpCa.txt", "w");
    F->ICab_ = fopen("./output_iso/ICab.txt", "w");
    F->Ist_ = fopen("./output_iso/Ist.txt", "w");
    F->dt_ = fopen("./output_iso/dt.txt", "w");
    F->ENa_ = fopen("./output_iso/ENa.txt", "w");
    F->EK_ = fopen("./output_iso/EK.txt", "w");
    F->EKs_ = fopen("./output_iso/EKs.txt", "w");

// for ICaL
    F->f_ = fopen("./output_iso/f.txt", "w");
    F->d_ = fopen("./output_iso/d.txt", "w");
    F->nca_ = fopen("./output_iso/nca.txt", "w");
    F->fca_ = fopen("./output_iso/fca.txt", "w");
    F->jca_ = fopen("./output_iso/jca.txt", "w");
    F->fp_ = fopen("./output_iso/fp.txt", "w");
    F->fcap_ = fopen("./output_iso/fcap.txt", "w");
    F->tff_ = fopen("./output_iso/tff.txt", "w");
    F->tfs_ = fopen("./output_iso/tds.txt", "w");
    F->tffp_ = fopen("./output_iso/tffp.txt", "w");
    F->dss_ = fopen("./output_iso/dss.txt", "w");
    F->fss_ = fopen("./output_iso/fss.txt", "w");
    F->Ical_V = fopen("./output_iso/ICaL_V.txt", "w");
//
}

void print_to_files(Files *F, State *St,
                    Variables *Var, Parameters *Par,
                    int chain_length) {

    fprintf(F->v_, "%e\t", Par->t);
    fprintf(F->nai_, "%e\t", Par->t);
    fprintf(F->nass_, "%e\t", Par->t);
    fprintf(F->ki_, "%e\t", Par->t);
    fprintf(F->kss_, "%e\t", Par->t);
    fprintf(F->cai_, "%e\t", Par->t);
    fprintf(F->cass_, "%e\t", Par->t);
    fprintf(F->cansr_, "%e\t", Par->t);
    fprintf(F->cajsr_, "%e\t", Par->t);
    fprintf(F->Jrel_, "%e\t", Par->t);
    fprintf(F->CaMKt_, "%e\t", Par->t);
    fprintf(F->Jup_, "%e\t", Par->t);
    fprintf(F->Jtr_, "%e\t", Par->t);
    fprintf(F->Jdiff_, "%e\t", Par->t);
    fprintf(F->JdiffNa_, "%e\t", Par->t);
    fprintf(F->JdiffK_, "%e\t", Par->t);
    fprintf(F->Jleak_, "%e\t", Par->t);
    fprintf(F->INa_, "%e\t", Par->t);
    fprintf(F->INaL_, "%e\t", Par->t);
    fprintf(F->Ito_, "%e\t", Par->t);
    fprintf(F->ICaL_, "%e\t", Par->t);
    fprintf(F->IKatp_, "%e\t", Par->t);
    fprintf(F->ICaNa_, "%e\t", Par->t);
    fprintf(F->ICaK_, "%e\t", Par->t);
    fprintf(F->IKr_, "%e\t", Par->t);
    fprintf(F->IKs_, "%e\t", Par->t);
    fprintf(F->IK1_, "%e\t", Par->t);
    fprintf(F->INaCa_i_, "%e\t", Par->t);
    fprintf(F->INaCa_ss_, "%e\t", Par->t);
    fprintf(F->INaCa_, "%e\t", Par->t);
    fprintf(F->INaK_, "%e\t", Par->t);
    fprintf(F->IKb_, "%e\t", Par->t);
    fprintf(F->INab_, "%e\t", Par->t);
    fprintf(F->IpCa_, "%e\t", Par->t);
    fprintf(F->ICab_, "%e\t", Par->t);
    fprintf(F->Ist_, "%e\t", Par->t);
    fprintf(F->ENa_, "%e\t", Par->t);
    fprintf(F->EK_, "%e\t", Par->t);
    fprintf(F->EKs_, "%e\t", Par->t);

    // for Var->ICaL
    fprintf(F->f_, "%e\t", Par->t);
    fprintf(F->d_, "%e\t", Par->t);
    fprintf(F->nca_, "%e\t", Par->t);
    fprintf(F->fca_, "%e\t", Par->t);
    fprintf(F->jca_, "%e\t", Par->t);
    fprintf(F->fp_, "%e\t", Par->t);
    fprintf(F->fcap_, "%e\t", Par->t);
    fprintf(F->tff_, "%e\t", Par->t);
    fprintf(F->tfs_, "%e\t", Par->t);
    fprintf(F->tffp_, "%e\t", Par->t);
    //fprintf(F->dss_,"%e\t",St[i].v);
    // fprintf(F->fss_,"%e\t",St[i].v);

    for (int i = 0; i < chain_length; i++) {
        fprintf(F->v_, "%e\t", St[i].v);
        fprintf(F->nai_, "%e\t", St[i].nai);
        fprintf(F->nass_, "%e\t", St[i].nass);
        fprintf(F->ki_, "%e\t", St[i].ki);
        fprintf(F->kss_, "%e\t", St[i].kss);
        fprintf(F->cai_, "%e\t", St[i].cai);
        fprintf(F->cass_, "%e\t", St[i].cass);
        fprintf(F->cansr_, "%e\t", St[i].cansr);
        fprintf(F->cajsr_, "%e\t", St[i].cajsr);
        fprintf(F->Jrel_, "%e\t", Var->Jrel);
        fprintf(F->CaMKt_, "%e\t", St[i].CaMKt);
        fprintf(F->Jup_, "%e\t", Var->Jup);
        fprintf(F->Jtr_, "%e\t", Var->Jtr);
        fprintf(F->Jdiff_, "%e\t", Var->Jdiff);
        fprintf(F->JdiffNa_, "%e\t", Var->JdiffNa);
        fprintf(F->JdiffK_, "%e\t", Var->JdiffK);
        fprintf(F->Jleak_, "%e\t", Var->Jleak);
        fprintf(F->INa_, "%e\t", Var->INa);
        fprintf(F->INaL_, "%e\t", Var->INaL);
        fprintf(F->Ito_, "%e\t", Var->Ito);
        fprintf(F->ICaL_, "%e\t", Var->ICaL);
        fprintf(F->ICaNa_, "%e\t", Var->ICaNa);
        fprintf(F->ICaK_, "%e\t", Var->ICaK);
        fprintf(F->IKr_, "%e\t", Var->IKr);
        fprintf(F->IKs_, "%e\t", Var->IKs);
        fprintf(F->IK1_, "%e\t", Var->IK1);
        fprintf(F->INaCa_i_, "%e\t", Var->INaCa_i);
        fprintf(F->INaCa_ss_, "%e\t", Var->INaCa_ss);
        fprintf(F->INaCa_, "%e\t", Var->INaCa);
        fprintf(F->INaK_, "%e\t", Var->INaK);
        fprintf(F->IKb_, "%e\t", Var->IKb);
        fprintf(F->INab_, "%e\t", Var->INab);
        fprintf(F->IpCa_, "%e\t", Var->IpCa);
        fprintf(F->ICab_, "%e\t", Var->ICab);
        fprintf(F->IKatp_, "%e\t", Var->IKatp);
        fprintf(F->Ist_, "%e\t", Var->Ist);
        fprintf(F->dt_, "%e\n", Par->dt);
        fprintf(F->ENa_, "%e\t", Var->ENa);
        fprintf(F->EK_, "%e\t", Var->EK);
        fprintf(F->EKs_, "%e\t", Var->EKs);
        fprintf(F->dss_, "%e\t %e\t", St[i].v, Var->dss);
        fprintf(F->fss_, "%e\t %e\t", St[i].v, Var->fss);
        if (St[i].v != -70) {
            fprintf(F->Ical_V, "%e\t %e\t", St[i].v, Var->ICaL);
        }
        // for Var->ICaL
        /*fprintf(F->f_,"%e\t",f);
        fprintf(F->d_,"%e\t",St[i].d);
        fprintf(F->nca_,"%e\t",St[i].nca);
        fprintf(F->fca_,"%e\t",fca);
        fprintf(F->jca_,"%e\t",St[i].jca);
        fprintf(F->fp_,"%e\t",fp);
        fprintf(F->fcap_,"%e\t",fcap);
        fprintf(F->tff_,"%e\t",tff);
        fprintf(F->tfs_,"%e\t",tfs);
        fprintf(F->tffp_,"%e\t",tffp);*/
    }

    fprintf(F->v_, "\n");
    fprintf(F->nai_, "\n");
    fprintf(F->nass_, "\n");
    fprintf(F->ki_, "\n");
    fprintf(F->kss_, "\n");
    fprintf(F->cai_, "\n");
    fprintf(F->cass_, "\n");
    fprintf(F->cansr_, "\n");
    fprintf(F->cajsr_, "\n");
    fprintf(F->Jrel_, "\n");
    fprintf(F->CaMKt_, "\n");
    fprintf(F->Jup_, "\n");
    fprintf(F->Jtr_, "\n");
    fprintf(F->Jdiff_, "\n");
    fprintf(F->JdiffNa_, "\n");
    fprintf(F->JdiffK_, "\n");
    fprintf(F->Jleak_, "\n");
    fprintf(F->INa_, "\n");
    fprintf(F->INaL_, "\n");
    fprintf(F->Ito_, "\n");
    fprintf(F->ICaL_, "\n");
    fprintf(F->ICaNa_, "\n");
    fprintf(F->ICaK_, "\n");
    fprintf(F->IKr_, "\n");
    fprintf(F->IKs_, "\n");
    fprintf(F->IK1_, "\n");
    fprintf(F->INaCa_i_, "\n");
    fprintf(F->INaCa_ss_, "\n");
    fprintf(F->INaCa_, "\n");
    fprintf(F->INaK_, "\n");
    fprintf(F->IKb_, "\n");
    fprintf(F->INab_, "\n");
    fprintf(F->IpCa_, "\n");
    fprintf(F->ICab_, "\n");
    fprintf(F->IKatp_, "\n");
    fprintf(F->Ist_, "\n");
    fprintf(F->ENa_, "\n");
    fprintf(F->EK_, "\n");
    fprintf(F->EKs_, "\n");
    // for Var->ICaL
    fprintf(F->f_, "\n");
    fprintf(F->d_, "\n");
    fprintf(F->nca_, "\n");
    fprintf(F->fca_, "\n");
    fprintf(F->jca_, "\n");
    fprintf(F->fp_, "\n");
    fprintf(F->fcap_, "\n");
    fprintf(F->tff_, "\n");
    fprintf(F->tfs_, "\n");
    fprintf(F->tffp_, "\n");
    fprintf(F->dss_, "\n");
    fprintf(F->fss_, "\n");
    fprintf(F->Ical_V, "\n");
    return;
}

void close(Files *F) {
    fclose(F->t_);
    fclose(F->v_);
    fclose(F->nai_);
    fclose(F->nass_);
    fclose(F->ki_);
    fclose(F->kss_);
    fclose(F->cai_);
    fclose(F->cass_);
    fclose(F->cansr_);
    fclose(F->cajsr_);
    fclose(F->Jrel_);
    fclose(F->CaMKt_);
    fclose(F->Jup_);
    fclose(F->Jtr_);
    fclose(F->Jdiff_);
    fclose(F->JdiffNa_);
    fclose(F->JdiffK_);
    fclose(F->Jleak_);
    fclose(F->INa_);
    fclose(F->INaL_);
    fclose(F->Ito_);
    fclose(F->ICaL_);
    fclose(F->ICaNa_);
    fclose(F->ICaK_);
    fclose(F->IKr_);
    fclose(F->IKs_);
    fclose(F->IK1_);
    fclose(F->INaCa_i_);
    fclose(F->INaCa_ss_);
    fclose(F->INaCa_);
    fclose(F->INaK_);
    fclose(F->IKb_);
    fclose(F->INab_);
    fclose(F->IpCa_);
    fclose(F->ICab_);
    fclose(F->IKatp_);
    fclose(F->Ist_);
    fclose(F->dt_);
    fclose(F->ENa_);
    fclose(F->EK_);
    fclose(F->EKs_);

// for ICaL
    fclose(F->f_);
    fclose(F->d_);
    fclose(F->nca_);
    fclose(F->fca_);
    fclose(F->jca_);
    fclose(F->fp_);
    fclose(F->fcap_);
    fclose(F->tff_);
    fclose(F->tfs_);
    fclose(F->tffp_);
    fclose(F->dss_);
    fclose(F->fss_);
    fclose(F->Ical_V);
//

}

void create_header_csv(std::ofstream &file_csv) {

    file_csv << "t,dt,CL,v,nai,nass,ki,kss,cai,cass,cansr,cajsr,Jrel,CaMKt,Jup,Jtr,Jdiff,JdiffNa,JdiffK,Jleak,";
    file_csv << "INa,INaL,Ito,ICaL,IKatp,ICaNa,ICaK,IKr,IKs,IK1,INaCa_i,INaCa_ss,INaCa,INaK,IKb,INab,IpCa,ICab,Ist,I_gap_junc,";
    file_csv << "ENa,EK,EKs";
    file_csv << std::endl;
};

void print_to_scv(State *St, Variables *Var, Parameters *Par, std::ofstream &file_csv) {

    file_csv.precision(5);
    file_csv << std::scientific;
    file_csv << Par->t << "," << Par->dt << "," << Par->CL << "," << St->v << "," << St->nai << "," << St->nass << ","
             << St->ki << "," << St->kss << "," << St->cai << "," << St->cass << "," << St->cansr << "," << St->cajsr
             << "," << Var->Jrel << "," << St->CaMKt << "," << Var->Jup << "," << Var->Jtr << "," << Var->Jdiff << ","
             << Var->JdiffNa << "," << Var->JdiffK << "," << Var->Jleak << "," << Var->INa << "," << Var->INaL << ","
             << Var->Ito << "," << Var->ICaL << "," << Var->IKatp << "," << Var->ICaNa << "," << Var->ICaK << ","
             << Var->IKr << "," << Var->IKs << "," << Var->IK1 << "," << Var->INaCa_i << "," << Var->INaCa_ss << ","
             << Var->INaCa << "," << Var->INaK << "," << Var->IKb << "," << Var->INab << "," << Var->IpCa << ","
             << Var->ICab << "," << Var->Ist << "," << Var->I_gap_junc << "," << Var->ENa << "," << Var->EK << ","
             << Var->EKs << std::endl;
}