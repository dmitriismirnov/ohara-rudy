//
// Created by andrey on 30/03/19.
//

#ifndef OHARA_RUDY_CONSOLEOUTPUT_H
#define OHARA_RUDY_CONSOLEOUTPUT_H

#include <iostream>
#include <chrono>
#include <thread>

#include "Parameters.h"

#define CYAN_FG         "\u001b[36m"
#define BOLD            "\u001b[1m"
#define RESET_STYLE     "\u001b[0m"
#define CLEAR_LINE	    "\u001b[2K"


class ConsolePrinter {

public:
    ConsolePrinter() : filled_lines_count(0), beats_printed(0) {};

    void print(Parameters *Par);

    void skip_filled_lines();

private:

    void print_progress(Parameters *Par);

    void print_CL(Parameters *Par);

    int filled_lines_count;

    int beats_printed;

};

inline void ConsolePrinter::skip_filled_lines() {
    std::cout << "\u001b[" << filled_lines_count << "E";
    std::cout << "\u001b[100C\n";
}


inline void ConsolePrinter::print_CL(Parameters *Par) {
    std::cout << "CL = ";
    if (!Par->DECREASE_CL || Par->CL == Par->CL_final) {
        std::cout << BOLD;
    }
    std::cout << Par->CL << RESET_STYLE << " (ms)";
}


inline void ConsolePrinter::print_progress(Parameters *Par) {
    std::cout << "Progress: " << int(1000 * Par->t / Par->ft) / 10. << "%";
}


inline void ConsolePrinter::print(Parameters *Par) {

    if (Par->beats_count == beats_printed) {
        return;
    }

    beats_printed = Par->beats_count;
    filled_lines_count = 0;

    std::cout << CLEAR_LINE;
    print_CL(Par);
    std::cout << "\n";
    filled_lines_count++;

    std::cout << CLEAR_LINE;
    print_progress(Par);

    if (Par->beats_count > Par->number_of_beats - Par->beats_save) {
        std::cout << "\n" << CYAN_FG << BOLD << "Saving..." << RESET_STYLE;
        filled_lines_count++;
    }

    std::cout << "\u001b[" << filled_lines_count << "F";
}


#endif //OHARA_RUDY_CONSOLEOUTPUT_H

