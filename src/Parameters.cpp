//
// Created by andrey on 03/04/19.
//

#include "Parameters.h"

Parameters::Parameters(double const dt, double const safe_time, double const output_step, int const beats_save,
                              int const chain_length, double const amplitude, double const start, double const duration,
                              int const number_of_beats, double const CL_initial, bool const DECREASE_CL,
                              double const CL_final, double const bpm_step, int const beats_final) :

        dt(dt), t(0), safe_time(safe_time),

        output_step(output_step), skip(int(output_step / dt)),
        count(0),

        chain_length(chain_length),

        amplitude(amplitude), start(start), duration(duration),
        beats_count(0), BEATS_COUNT_MUTEX(true), t_stimulus(start),

        CL_initial(CL_initial), CL(CL_initial),

        DECREASE_CL(DECREASE_CL),
        CL_final(CL_final), bpm_step(bpm_step),
        beats_final(beats_final){


    if (DECREASE_CL) {
        if (CL_initial < CL_final) {
            std::string report = "CL_initial must be less than CL_final";
            throw std::runtime_error(report);
        } else if (CL_initial > CL_final) {
            if (bpm_step <= 0) {
                std::string report = "Invalid bpm_step";
                throw std::runtime_error(report);
            }
        }

        this->number_of_beats = 0;
        ft = 0;

        double CL_ = CL_initial;
        while (CL_ > CL_final) {
            double bpm = 60. / (CL_ / 1000);
            double bpm_next = bpm + bpm_step;
            CL_ = 1000 * 60. / bpm_next;
            if (CL_ < CL_final) {
                CL_ = CL_final;
            }
            this->number_of_beats += 1;
            ft += CL_;
        }

        this->number_of_beats += beats_final;
        ft += beats_final * CL_final;

    } else {

        this->number_of_beats = number_of_beats;
        ft = number_of_beats * CL_initial;
    }

    this->beats_save = (beats_save >= 0) ? beats_save : this->number_of_beats;

}


void Parameters::update_stimulus() {
    if (DECREASE_CL) {
        if (CL > CL_final) {
            double bpm = 60. / (CL / 1000); // current frequency
            double bpm_next = bpm + bpm_step;
            CL = 1000 * 60. / bpm_next; // current CL
            if (CL < CL_final) {
                CL = CL_final;
            }
        }
    }
    t_stimulus += CL;
    BEATS_COUNT_MUTEX = true;
}


void Parameters::update_beats_count() {
    if (BEATS_COUNT_MUTEX) {
        beats_count += 1;
        BEATS_COUNT_MUTEX = false;
    }
}