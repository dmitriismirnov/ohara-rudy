//
// Created by andrey on 02/12/18.
//

#ifndef OHARA_RUDY_INIT_H
#define OHARA_RUDY_INIT_H

#include <string>
#include <iostream>
#include "Constants.h"
#include "Parameters.h"
#include "ConfigFile.h"
#include "State.h"

Constants* load_constants_from_file(std::string const& filename);

Parameters* load_parameters_from_file(std::string const& filename);

void initialize_state(State *St);

void update_state_from_file(State *St, std::string filename);

//void initialization(struct State *St);

#endif //OHARA_RUDY_INIT_H

