//
// Created by andrey on 03/12/18.
//

#ifndef OHARA_RUDY_FILEWORK_H
#define OHARA_RUDY_FILEWORK_H

#include <cstdio>
#include <fstream>
#include "State.h"
#include "Variables.h"
#include "Parameters.h"

struct Files {

    FILE *t_, *v_, *nai_, *nass_, *ki_, *kss_, *cai_, *cass_, *cansr_, *cajsr_, *Jrel_, *CaMKt_, *Jup_, *Jtr_, *Jdiff_,
            *JdiffNa_, *JdiffK_, *Jleak_, *INa_, *INaL_, *Ito_, *ICaL_, *ICaNa_, *ICaK_, *IKr_, *IKs_, *IK1_, *INaCa_i_,
            *INaCa_ss_, *INaCa_, *INaK_, *IKb_, *INab_, *IpCa_, *ICab_, *Ist_, *dt_, *APD_, *ENa_, *EK_, *EKs_, *f_, *d_,
            *nca_, *fca_, *jca_, *fp_, *fcap_, *tff_, *tfs_, *tffp_, *dss_, *fss_, *Ical_V, *IKatp_;
};

void open(Files *F);

void open_iso(Files *F);

void print_to_files(Files * F, State *St, Variables *Var, Parameters *Par, int chain_length);

void close(Files *F);

void create_header_csv(std::ofstream & file_csv);

void print_to_scv(State *St, Variables *Var, Parameters *Par, std::ofstream& file_csv);

#endif //OHARA_RUDY_FILEWORK_H
