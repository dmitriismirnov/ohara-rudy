//
// Created by andrey on 04/12/18.
//


#ifndef OHARA_RUDY_PARAMETERS_H
#define OHARA_RUDY_PARAMETERS_H

#include <stdexcept>
#include <string>
#include <iostream>

struct Parameters {

    Parameters(
            ///times
            double dt, double safe_time,

            ///output
            double output_step, int beats_save,

            ///sizes
            int chain_length,

            ///stimulation
            double amplitude,
            double start,
            double duration,
            int number_of_beats,
            double CL_initial,

            bool DECREASE_CL,
            double CL_final,
            double bpm_step,
            int beats_final);


    void update_stimulus();

    void update_beats_count();


    ///times
    double dt;
    double ft;
    double t;
    double const safe_time;

    ///output
    double const output_step;
    int const skip;
    int count;
    int beats_save;

    ///chain
    int const chain_length;

    ///stimulation
    double const amplitude;
    double const start;
    double const duration;
    int number_of_beats;
    int beats_count;
    bool BEATS_COUNT_MUTEX;
    double t_stimulus;

    double const CL_initial;
    double CL;
    bool const DECREASE_CL;
    double const CL_final;
    double const bpm_step;
    int const beats_final;
};


#endif //OHARA_RUDY_PARAMETERS_H
