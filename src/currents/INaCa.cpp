//
// Created by andrey on 06/12/18.
//

#include "INaCa.h"

void update_INaCa(State *St, Variables *Var, Constants *Cnst){

    double kna1 = 15.0;
    double kna2 = 5.0;
    double kna3 = 88.12;
    double kasymm = 12.5;
    double wna = 6.0e4;
    double wca = 6.0e4;
    double wnaca = 5.0e3;
    double kcaon = 1.5e6;
    double kcaoff = 5.0e3;
    double qna = 0.5224;
    double qca = 0.1670;
    double hca = exp((qca * St->v * F) / (R * T));
    double hna = exp((qna * St->v * F) / (R * T));
    double h1 = 1 + St->nai / kna3 * (1 + hna);
    double h2 = (St->nai * hna) / (kna3 * h1);
    double h3 = 1.0 / h1;
    double h4 = 1.0 + St->nai / kna1 * (1 + St->nai / kna2);
    double h5 = St->nai * St->nai / (h4 * kna1 * kna2);
    double h6 = 1.0 / h4;
    double h7 = 1.0 + Cnst->nao / kna3 * (1.0 + 1.0 / hna);
    double h8 = Cnst->nao / (kna3 * hna * h7);
    double h9 = 1.0 / h7;
    double h10 = kasymm + 1.0 + Cnst->nao / kna1 * (1.0 + Cnst->nao / kna2);
    double h11 = Cnst->nao * Cnst->nao / (h10 * kna1 * kna2);
    double h12 = 1.0 / h10;
    double k1 = h12 * Cnst->cao * kcaon;
    double k2 = kcaoff;
    double k3p = h9 * wca;
    double k3pp = h8 * wnaca;
    double k3 = k3p + k3pp;
    double k4p = h3 * wca / hca;
    double k4pp = h2 * wnaca;
    double k4 = k4p + k4pp;
    double k5 = kcaoff;
    double k6 = h6 * St->cai * kcaon;
    double k7 = h5 * h2 * wna;
    double k8 = h8 * h11 * wna;
    double x1 = k2 * k4 * (k7 + k6) + k5 * k7 * (k2 + k3);
    double x2 = k1 * k7 * (k4 + k5) + k4 * k6 * (k1 + k8);
    double x3 = k1 * k3 * (k7 + k6) + k8 * k6 * (k2 + k3);
    double x4 = k2 * k8 * (k4 + k5) + k3 * k5 * (k1 + k8);
    double E1 = x1 / (x1 + x2 + x3 + x4);
    double E2 = x2 / (x1 + x2 + x3 + x4);
    double E3 = x3 / (x1 + x2 + x3 + x4);
    double E4 = x4 / (x1 + x2 + x3 + x4);
    double KmCaAct = 150.0e-6;
    double allo = 1.0 / (1.0 + pow(KmCaAct / St->cai, 2.0));
    double zna = 1.0;
    double JncxNa = 3.0 * (E4 * k7 - E1 * k8) + E3 * k4pp - E2 * k3pp;
    double JncxCa = E2 * k2 - E1 * k1;
    double Gncx = 0.0008 * Cnst->c_gncx;
    if (Cnst->celltype == 1) {
        Gncx *= 1.1;
    }
    if (Cnst->celltype == 2) {
        Gncx *= 1.4;
    }

    double zca = 2.0;
    Var->INaCa_i = 0.8 * Gncx * allo * (zna * JncxNa + zca * JncxCa);

    h1 = 1 + St->nass / kna3 * (1 + hna);
    h2 = (St->nass * hna) / (kna3 * h1);
    h3 = 1.0 / h1;
    h4 = 1.0 + St->nass / kna1 * (1 + St->nass / kna2);
    h5 = St->nass * St->nass / (h4 * kna1 * kna2);
    h6 = 1.0 / h4;
    h7 = 1.0 + Cnst->nao / kna3 * (1.0 + 1.0 / hna);
    h8 = Cnst->nao / (kna3 * hna * h7);
    h9 = 1.0 / h7;
    h10 = kasymm + 1.0 + Cnst->nao / kna1 * (1 + Cnst->nao / kna2);
    h11 = Cnst->nao * Cnst->nao / (h10 * kna1 * kna2);
    h12 = 1.0 / h10;
    k1 = h12 * Cnst->cao * kcaon;
    k2 = kcaoff;
    k3p = h9 * wca;
    k3pp = h8 * wnaca;
    k3 = k3p + k3pp;
    k4p = h3 * wca / hca;
    k4pp = h2 * wnaca;
    k4 = k4p + k4pp;
    k5 = kcaoff;
    k6 = h6 * St->cass * kcaon;
    k7 = h5 * h2 * wna;
    k8 = h8 * h11 * wna;
    x1 = k2 * k4 * (k7 + k6) + k5 * k7 * (k2 + k3);
    x2 = k1 * k7 * (k4 + k5) + k4 * k6 * (k1 + k8);
    x3 = k1 * k3 * (k7 + k6) + k8 * k6 * (k2 + k3);
    x4 = k2 * k8 * (k4 + k5) + k3 * k5 * (k1 + k8);
    E1 = x1 / (x1 + x2 + x3 + x4);
    E2 = x2 / (x1 + x2 + x3 + x4);
    E3 = x3 / (x1 + x2 + x3 + x4);
    E4 = x4 / (x1 + x2 + x3 + x4);
    KmCaAct = 150.0e-6;
    allo = 1.0 / (1.0 + pow(KmCaAct / St->cass, 2.0));
    JncxNa = 3.0 * (E4 * k7 - E1 * k8) + E3 * k4pp - E2 * k3pp;
    JncxCa = E2 * k2 - E1 * k1;
    Var->INaCa_ss = 0.2 * Gncx * allo * (zna * JncxNa + zca * JncxCa);

    Var->INaCa = Var->INaCa_i + Var->INaCa_ss;
}