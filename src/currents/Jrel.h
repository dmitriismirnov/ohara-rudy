//
// Created by andrey on 05/12/18.
//

#ifndef INTEGRATION_JREL_H
#define INTEGRATION_JREL_H


#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Parameters.h"
#include "../Constants.h"

void update_Jrel(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst);

#endif //INTEGRATION_JREL_H
