//
// Created by andrey on 05/12/18.
//

#include "IKs.h"

void update_IKs(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                Table *Tbl) {

    double xs1ss, txs1, txs2;

    if (Tbl != nullptr) {
        int ip; // integer part
        double fp; // fractional part
        Tbl->get_int_frac(CurrentSt->v, ip, fp);
        INTERPOLATE(Tbl, xs1ss, ip, fp);
        INTERPOLATE(Tbl, txs1, ip, fp);
        INTERPOLATE(Tbl, txs2, ip, fp);
    } else {
        xs1ss = calculate_xs1ss(CurrentSt->v);
        txs1 = calculate_txs1(CurrentSt->v);
        txs2 = calculate_txs2(CurrentSt->v);
    }

    double xs2ss = xs1ss;
    double GKs = 0.0034 * Cnst->c_gks;
    double pdgn = 1;

    if (Cnst->ISO == true)
    {
        txs1 *= 5.475 * Cnst->c_txs_1_iso;
        GKs *= 3.2 * Cnst->c_IKs_ISO;
        pdgn *= 1.;
    }

    if (Cnst->celltype == 1)
    {
        GKs *= 1.4;
    }

    NextSt->xs1 = xs1ss - (xs1ss - CurrentSt->xs1) * exp(-Par->dt / txs1);
    NextSt->xs2 = xs2ss - (xs2ss - CurrentSt->xs2) * exp(-Par->dt / txs2);

    double KsCa = 1.0 + 0.6 / (1.0 + pow(3.8e-5 / CurrentSt->cai, 1.4));

    Var->IKs = pdgn * GKs * KsCa * CurrentSt->xs1 * CurrentSt->xs2 * (CurrentSt->v - Var->EKs);

}