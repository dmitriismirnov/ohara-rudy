//
// Created by andrey on 06/12/18.
//

#include "INaL.h"

void update_INaL(State *CurrentSt, State *NextSt,
                 Variables *Var, Parameters *Par, Constants *Cnst,
                 Table *Tbl) {

    double mLss, hLss, hLssp, tmL;

    if (Tbl != nullptr) {

        int ip; // integer part
        double fp; // fractional part
        Tbl->get_int_frac(CurrentSt->v, ip, fp);

        INTERPOLATE(Tbl, mLss, ip, fp);
        INTERPOLATE(Tbl, hLss, ip, fp);
        INTERPOLATE(Tbl, hLssp, ip, fp);
        INTERPOLATE(Tbl, tmL, ip, fp);

    } else {
        mLss = calculate_mLss(CurrentSt->v);
        hLss = calculate_hLss(CurrentSt->v);
        hLssp = calculate_hLssp(CurrentSt->v);
        tmL = calculate_tmL(CurrentSt->v);
    }

    double thL = 200.0;
    double thLp = 3.0 * thL;

    NextSt->mL = mLss - (mLss - CurrentSt->mL) * exp(-Par->dt / tmL);
    NextSt->hL = hLss - (hLss - CurrentSt->hL) * exp(-Par->dt / thL);
    NextSt->hLp = hLssp - (hLssp - CurrentSt->hLp) * exp(-Par->dt / thLp);

    double GNaL = 0.0075 * Cnst->c_gnal;
    if (Cnst->celltype == 1) {
        GNaL *= 0.6;
    }
    double fINaLp = (1.0 / (1.0 + Cnst->KmCaMK / Var->CaMKa));
    Var->INaL = GNaL * (CurrentSt->v - Var->ENa) * CurrentSt->mL *
                ((1.0 - fINaLp) * CurrentSt->hL + fINaLp * CurrentSt->hLp);
}