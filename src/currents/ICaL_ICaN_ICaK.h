//
// Created by andrey on 06/12/18.
//

#ifndef INTEGRATION_ICAL_ICAN_ICAK_H
#define INTEGRATION_ICAL_ICAN_ICAK_H

#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Parameters.h"
#include "../Constants.h"
#include "../Tables.h"
#include "subroutine.h"

void update_ICaL_ICaN_ICaK(State *CurrentSt, State *NextSt,
                 Variables *Var, Parameters *Par, Constants *Cnst,
                           Table *Tbl = nullptr);

#endif //INTEGRATION_ICAL_ICAN_ICAK_H
