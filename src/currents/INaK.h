//
// Created by andrey on 06/12/18.
//

#ifndef INTEGRATION_INAK_H
#define INTEGRATION_INAK_H

#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Constants.h"

void update_INaK(State *St, Variables *Var, Constants *Cnst);

#endif //INTEGRATION_INAK_H
