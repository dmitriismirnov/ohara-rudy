//
// Created by andrey on 06/12/18.
//

#include "Jup.h"

void update_Jup(State *St, Variables *Var, Constants *Cnst) {
    double Jupnp;
    double Jupp;
    if (Cnst->ISO == true) {
        Jupnp = Cnst->c_jup * 0.004375 * St->cai / (St->cai + 0.00092 * 0.54 * Cnst->c_SERCA);
    } else {
        Jupnp = Cnst->c_jup * 0.004375 * St->cai / (St->cai + 0.00092);
    }


    if (Cnst->ISO == true) {
        Jupp = Cnst->c_jup * 2.75 * 0.004375 * St->cai /
               (St->cai + (0.00092 - 0.00017) * 0.54 * Cnst->c_SERCA);
    } else {
        Jupp = Cnst->c_jup * 2.75 * 0.004375 * St->cai / (St->cai + 0.00092 - 0.00017);
    }
    if (Cnst->celltype == 1) {
        Jupnp *= 1.3;
        Jupp *= 1.3;
    }
    double fJupp = (1.0 / (1.0 + Cnst->KmCaMK / Var->CaMKa));

    Var->Jup = (1.0 - fJupp) * Jupnp + fJupp * Jupp - Var->Jleak;
}