//
// Created by andrey on 05/12/18.
//

#include "IK1.h"

void update_IK1(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                Table *Tbl) {

    double xk1ss, txk1, rk1;

    if (Tbl != nullptr) {
        int ip; // integer part
        double fp; // fractional part
        Tbl->get_int_frac(CurrentSt->v, ip, fp);
        INTERPOLATE(Tbl, xk1ss, ip, fp);
        INTERPOLATE(Tbl, txk1, ip, fp);
        INTERPOLATE(Tbl, rk1, ip, fp);
    } else {
        xk1ss = calculate_xk1ss(CurrentSt->v, Cnst->ko);
        txk1 = calculate_txk1(CurrentSt->v);
        rk1 = calculate_rk1(CurrentSt->v, Cnst->ko);
    }

    NextSt->xk1 = xk1ss - (xk1ss - CurrentSt->xk1) * exp(-Par->dt / txk1);

    double GK1 = 0.1908 * Cnst->c_gk1;
    if (Cnst->celltype == 1)
    {
        GK1 *= 1.2;
    }
    if (Cnst->celltype == 2)
    {
        GK1 *= 1.3;
    }

    Var->IK1 = GK1 * sqrt(Cnst->ko) * rk1 * CurrentSt->xk1 * (CurrentSt->v - Var->EK);
}