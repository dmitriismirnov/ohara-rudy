//
// Created by andrey on 05/12/18.
//

#ifndef INTEGRATION_INA_H
#define INTEGRATION_INA_H

#include <cmath>
#include <iostream>

#include "../State.h"
#include "../Variables.h"
#include "../Parameters.h"
#include "../Constants.h"
#include "../Tables.h"
#include "subroutine.h"

void update_INa(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                Table *Tbl = nullptr);

#endif //INTEGRATION_INA_H
