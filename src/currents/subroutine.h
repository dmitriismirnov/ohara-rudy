//
// Created by andrey on 13/02/19.
//

#ifndef OHARA_RUDY_SUBROUTINE_H
#define OHARA_RUDY_SUBROUTINE_H

#include <cmath>

///* INa */
inline double calculate_mss(double v, double c_INa_dV_act = 0) {
    return 1.0 / (1.0 + exp((-(v + 39.57 + 3.7 * c_INa_dV_act)) / 9.871));
}

inline double calculate_tm(double v) {
    return 1.0 / (6.765 * exp((v + 11.64) / 34.77) +
                  8.552 * exp(-(v + 77.42) / 5.955));
}

inline double calculate_hss(double v, double c_INa_dV_inact = 0) {
    return 1.0 / (1 + exp((v + 82.90 + 4.9 * c_INa_dV_inact) / 6.086));
}

inline double calculate_thf(double v) {
    return 1.0 / (1.432e-5 * exp(-(v + 1.196) / 6.285) +
                  6.149 * exp((v + 0.5096) / 20.27));
}

inline double calculate_ths(double v) {
    return 1.0 / (0.009794 * exp(-(v + 17.95) / 28.05) +
                  0.3343 * exp((v + 5.730) / 56.66));
}

inline double calculate_tj(double v) {
    return 2.038 + 1.0 / (0.02136 * exp(-(v + 100.6) / 8.281) +
                          0.3052 * exp((v + 0.9941) / 38.45));
}

inline double calculate_hssp(double v, double c_INa_dV_inact = 0) {
    return 1.0 / (1 + exp((v + 89.1 + 4.9 * c_INa_dV_inact) / 6.086));
}


///* INaL */
inline double calculate_mLss(double v) {
    return 1.0 / (1.0 + exp((-(v + 42.85)) / 5.264));
}

inline double calculate_hLss(double v) {
    return 1.0 / (1.0 + exp((v + 87.61) / 7.488));
}

inline double calculate_hLssp(double v) {
    return 1.0 / (1.0 + exp((v + 93.81) / 7.488));
}

inline double calculate_tmL(double v) {
    return calculate_tm(v);
}


///* Ito */
inline double calculate_ass(double v) {
    return 1.0 / (1.0 + exp((-(v - 14.34)) / 14.82));
}

inline double calculate_ta(double v) {
    return 1.0515 / (1.0 / (1.2089 * (1.0 + exp(-(v - 18.4099) / 29.3814))) +
                     3.5 / (1.0 + exp((v + 100.0) / 29.3814)));
}

inline double calculate_iss(double v) {
    return 1.0 / (1.0 + exp((v + 43.94) / 5.711));
}

inline double calculate_tiF(double v, int celltype = 0) {

    double delta_epi;
    if (celltype == 1) {
        delta_epi = 1.0 - (0.95 / (1.0 + exp((v + 70.0) / 5.0)));
    } else {
        delta_epi = 1.0;
    }

    double tiF = 4.562 + 1 / (0.3933 * exp((-(v + 100.0)) / 100.0) +
                              0.08004 * exp((v + 50.0) / 16.59));
    tiF *= delta_epi;
    return tiF;
}

inline double calculate_tiS(double v, int celltype = 0) {

    double delta_epi;
    if (celltype == 1) {
        delta_epi = 1.0 - (0.95 / (1.0 + exp((v + 70.0) / 5.0)));
    } else {
        delta_epi = 1.0;
    }

    double tiS = 23.62 + 1 / (0.001416 * exp((-(v + 96.52)) / 59.05) +
                              1.780e-8 * exp((v + 114.1) / 8.079));
    tiS *= delta_epi;
    return tiS;
}

inline double calculate_assp(double v) {
    return 1.0 / (1.0 + exp((-(v - 24.34)) / 14.82));
}

inline double calculate_tiFp(double v, double tiF) {
    double dti_develop = 1.354 + 1.0e-4 / (exp((v - 167.4) / 15.89) + exp(-(v - 12.23) / 0.2154));
    double dti_recover = 1.0 - 0.5 / (1.0 + exp((v + 70.0) / 20.0));
    double tiFp = dti_develop * dti_recover * tiF;
    return tiFp;
}

inline double calculate_tiSp(double v, double tiS) {
    double dti_develop = 1.354 + 1.0e-4 / (exp((v - 167.4) / 15.89) + exp(-(v - 12.23) / 0.2154));
    double dti_recover = 1.0 - 0.5 / (1.0 + exp((v + 70.0) / 20.0));
    double tiSp = dti_develop * dti_recover * tiS;
    return tiSp;
}


///* ICaL, ICaNa, ICaK */
inline double calculate_td(double v) {
    return 0.6 + 1.0 / (exp(-0.05 * (v + 6.0)) + exp(0.09 * (v + 14.0)));
}

inline double calculate_dss(double v, double c_ICaL_dV_act = 0) {
    return 1.0 / (1.0 + exp((-(v + 3.940 + 9.9 * c_ICaL_dV_act)) / 4.230));
}

inline double calculate_fss(double v, double c_ICaL_dV_inact = 0) {
    return 1.0 / (1.0 + exp((v + 19.58 + 8.0 * c_ICaL_dV_inact) / 3.696));
}

inline double calculate_tff(double v) {
    return 7.0 + 1.0 / (0.0045 * exp(-(v + 20.0) / 10.0) +
                        0.0045 * exp((v + 20.0) / 10.0));
}

inline double calculate_tfs(double v) {
    return 1000.0 + 1.0 / (0.000035 * exp(-(v + 5.0) / 4.0) +
                           0.000035 * exp((v + 5.0) / 6.0));
}

inline double calculate_tfcaf(double v) {
    return 7.0 + 1.0 / (0.04 * exp(-(v - 4.0) / 7.0) +
                        0.04 * exp((v - 4.0) / 7.0));
}

inline double calculate_tfcas(double v) {
    return 100.0 + 1.0 / (0.00012 * exp(-v / 3.0) + 0.00012 * exp(v / 7.0));
}

inline double calculate_Afcaf(double v) {
    return 0.3 + 0.6 / (1.0 + exp((v - 10.0) / 10.0));
}


///* IKr */
inline double calculate_xrss(double v, bool IKR_GRANDI = false) {
    double xrss;
    if (IKR_GRANDI == true) {
        xrss = 1.0 / (1.0 + exp(-(v + 10.0) / 5.0));
    } else {
        xrss = 1.0 / (1.0 + exp((-(v + 8.337)) / 6.789));
    }
    return xrss;
}

inline double calculate_txrs(double v, bool IKR_GRANDI = false) {
    double txrs;
    if (IKR_GRANDI == true) {
        txrs = 550.0 / (1.0 + exp(-(v + 22.0) / 9.0)) * 6.0 / (1.0 + exp((v + 11.0) / 9.0)) +
               230.0 / (1.0 + exp((v + 40.0) / 20.0));
    } else {
        txrs = 1.865 + 1.0 / (0.06629 * exp((v - 34.70) / 7.355) +
                              1.128e-5 * exp((-(v - 29.74)) / 25.94));
    }
    return txrs;
}

inline double calculate_rkr(double v, bool IKR_GRANDI = false) {
    double rkr;
    if (IKR_GRANDI == true) {
        rkr = 1.0 / (1.0 + exp((v + 74.0) / 24.0));
    } else {
        rkr = 1.0 / (1.0 + exp((v + 55.0) / 75.0)) * 1.0 / (1.0 + exp((v - 10.0) / 30.0));
    }
    return rkr;
}

inline double calculate_txrf(double v) {
    return 12.98 + 1.0 / (0.3652 * exp((v - 31.66) / 3.869) +
                          4.123e-5 * exp((-(v - 47.78)) / 20.38));
}

inline double calculate_Axrf(double v) {
    return 1.0 / (1.0 + exp((v + 54.81) / 38.21));
}


///* IKs */
inline double calculate_xs1ss(double v) {
    return 1.0 / (1.0 + exp((-(v + 11.60)) / 8.932));
}

inline double calculate_txs1(double v) {
    return 817.3 + 1.0 / (2.326e-4 * exp((v + 48.28) / 17.80) +
                          0.001292 * exp((-(v + 210.0)) / 230.0));
}

inline double calculate_txs2(double v) {
    return 1.0 / (0.01 * exp((v - 50.0) / 20.0) + 0.0193 * exp((-(v + 66.54)) / 31.0));
}


///* IK1 */
inline double calculate_xk1ss(double v, double ko) {
    return 1.0 / (1.0 + exp(-(v + 2.5538 * ko + 144.59) / (1.5692 * ko + 3.8115)));
}

inline double calculate_txk1(double v) {
    return 122.2 / (exp((-(v + 127.2)) / 20.36) + exp((v + 236.8) / 69.33));
}

inline double calculate_rk1(double v, double ko) {
    return 1.0 / (1.0 + exp((v + 105.8 - 2.6 * ko) / 9.493));
}

#endif //OHARA_RUDY_SUBROUTINE_H
