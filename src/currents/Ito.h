//
// Created by andrey on 05/12/18.
//

#ifndef INTEGRATION_ITO_H
#define INTEGRATION_ITO_H

#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Parameters.h"
#include "../Constants.h"
#include "../Tables.h"
#include "subroutine.h"

void update_Ito(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                Table *Tbl = nullptr);

#endif //INTEGRATION_ITO_H
