//
// Created by andrey on 05/12/18.
//

#include "IKATP.h"

void update_IKATP(State *St, Variables *Var, Constants *Cnst) { // TODO: Table


    // B. Trenor, et al. (1998) Computer model of the effects of pinacidil on ATP-sensitive potassium current.
    // Proceedings of the 20th Annual International Conference of the IEEE Engineering in Medicine and Biology Society.
    /*
    double Hill = 1.925;

    double kd1 = 1.292e-8;
    double kd2 = 1.328e-1;
    double kd3 = 8.0e3;
    double kd4 = 4.448e-1;
    double koef11 = 1.871e-5;
    double koef1 = 7.533e-9;
    double koef13 = 2.506e-6;

    double f_atp = 1 - pow(Cnst->ATP_i, Hill) / (pow(Cnst->ATP_i, Hill) +
                                                 (koef1 * koef11 * koef13 + Cnst->ADP_i * koef11 * koef13 +
                                                  koef1 * koef13 * Cnst->P_conc + koef1 * Cnst->P_conc * Cnst->ADP_i) /
                                                 (koef1 * koef11 * koef13 / kd1 + Cnst->ADP_i * koef11 * koef13 / kd2 +
                                                  koef1 * koef13 * Cnst->P_conc / kd3 +
                                                  koef1 * Cnst->P_conc * Cnst->ADP_i / kd4));
    */

    if (false) {

        // Kazbanov IV, et al. (2014) Effect of Global Cardiac Ischemia on Human Ventricular Fibrillation:
        // Insights from a Multi-scale Mechanistic Model of the Human Heart. PLOS Computational Biology.
        double GKatp = 155; // nS/pF
        Var->IKatp = GKatp * Cnst->f_atp * pow(Cnst->ko / 5.4, 0.3) *
                     (1. / (40. + 3.5 * exp(0.025 * St->v)) * (St->v - Var->EK));
        Var->IKatp *= Cnst->c_gkatp;

    } else {

        // GHK flux equation
        double z_K = 1.;
        double numerator = St->ki * exp(z_K * St->v * FRT) - Cnst->ko;
        double denominator = exp(z_K * St->v * FRT) - 1.;

        // Mg block for fitting
        // Babenko AP, et. al. (1998) Reconstituted Human Cardiac KATP Channels
        double deltaMg = 0.32;
        double KhMg0 = 2.6;
        double KhMg = KhMg0 * exp(-2. * deltaMg * St->v * FRT);
        double Mg_i = 0.7;
        double f_Mg = 1. / (1. + Mg_i / KhMg);

        double P_K = 2.73e-5;
        Var->IKatp = Cnst->f_atp * f_Mg * P_K * (z_K * z_K) * St->v * FFRT * (numerator / denominator);
        Var->IKatp *= Cnst->c_gkatp;
    }
}

