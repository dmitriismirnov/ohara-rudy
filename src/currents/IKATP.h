//
// Created by andrey on 05/12/18.
//

#ifndef OHARA_RUDY_IKATP_H
#define OHARA_RUDY_IKATP_H

#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Constants.h"

void update_IKATP(State *St, Variables *Var, Constants *Cnst);

#endif //OHARA_RUDY_IKATP_H
