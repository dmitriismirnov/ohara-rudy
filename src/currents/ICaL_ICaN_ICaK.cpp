//
// Created by andrey on 06/12/18.
//

#include "ICaL_ICaN_ICaK.h"

void update_ICaL_ICaN_ICaK(State *CurrentSt, State *NextSt,
                           Variables *Var, Parameters *Par, Constants *Cnst,
                           Table *Tbl) {

    double td, dss, tff, tfs, fss, tfcaf, tfcas, Afcaf;

    if (Tbl != nullptr) {

        int ip; // integer part
        double fp; // fractional part
        Tbl->get_int_frac(CurrentSt->v, ip, fp);
        INTERPOLATE(Tbl, td, ip, fp);
        INTERPOLATE(Tbl, dss, ip, fp);
        INTERPOLATE(Tbl, tff, ip, fp);
        INTERPOLATE(Tbl, tfs, ip, fp);
        INTERPOLATE(Tbl, fss, ip, fp);
        INTERPOLATE(Tbl, tfcaf, ip, fp);
        INTERPOLATE(Tbl, tfcas, ip, fp);
        INTERPOLATE(Tbl, Afcaf, ip, fp);

    } else {

        if (Cnst->ISO == true) {
            dss = calculate_dss(CurrentSt->v, Cnst->c_ICaL_dV_act);
            fss = calculate_fss(CurrentSt->v, Cnst->c_ICaL_dV_inact);
        } else {
            dss = calculate_dss(CurrentSt->v);
            fss = calculate_fss(CurrentSt->v);
        }
        td = calculate_td(CurrentSt->v);
        tff = calculate_tff(CurrentSt->v);
        tfs = calculate_tfs(CurrentSt->v);
        tfcaf = calculate_tfcaf(CurrentSt->v);
        tfcas = calculate_tfcas(CurrentSt->v);
        Afcaf = calculate_Afcaf(CurrentSt->v);
    }

    Var->dss = dss;
    Var->fss = fss;

    double Aff = 0.6;
    double Afs = 1.0 - Aff;
    double f = Aff * CurrentSt->ff + Afs * CurrentSt->fs;
    double fcass = Var->fss;
    double Afcas = 1.0 - Afcaf;
    double fca = Afcaf * CurrentSt->fcaf + Afcas * CurrentSt->fcas;
    double tjca = 75.0;
    double tffp = 2.5 * tff;
    double fp = Aff * CurrentSt->ffp + Afs * CurrentSt->fs;
    double tfcafp = 2.5 * tfcaf;
    double fcap = Afcaf * CurrentSt->fcafp + Afcas * CurrentSt->fcas;
    double Kmn = 0.002;
    double k2n = 1000.0;
    double km2n = CurrentSt->jca * 1.0;
    double anca = 1.0 / (k2n / km2n + pow(1.0 + Kmn / CurrentSt->cass, 4.0));
    double PhiCaL;

    if (CurrentSt->cass < 0.03)
        PhiCaL = 4.0 * (CurrentSt->v * FFRT) *
                 (CurrentSt->cass * exp(2.0 * (CurrentSt->v * FRT)) - 0.341 * Cnst->cao) /
                 (exp(2.0 * (CurrentSt->v * FRT)) - 1.0);
    else PhiCaL = 4.0 * (CurrentSt->v * FFRT) * (0.03 * exp(2.0 * (CurrentSt->v * FRT)) - 0.341 * Cnst->cao) /
                  (exp(2.0 * (CurrentSt->v * FRT)) - 1.0);

    double PhiCaNa = 1.0 * (CurrentSt->v * FFRT) *
                     (0.75 * CurrentSt->nass * exp(1.0 * (CurrentSt->v * FRT)) - 0.75 * Cnst->nao) /
                     (exp(1.0 * (CurrentSt->v * FRT)) - 1.0);
    double PhiCaK =
            1.0 * (CurrentSt->v * FFRT) * (0.75 * CurrentSt->kss * exp(1.0 * (CurrentSt->v * FRT)) - 0.75 * Cnst->ko) /
            (exp(1.0 * (CurrentSt->v * FRT)) - 1.0);

    NextSt->d = Var->dss - (Var->dss - CurrentSt->d) * exp(-Par->dt / td);
    NextSt->ff = Var->fss - (Var->fss - CurrentSt->ff) * exp(-Par->dt / tff);
    NextSt->fs = Var->fss - (Var->fss - CurrentSt->fs) * exp(-Par->dt / tfs);
    NextSt->fcaf = fcass - (fcass - CurrentSt->fcaf) * exp(-Par->dt / tfcaf);
    NextSt->fcas = fcass - (fcass - CurrentSt->fcas) * exp(-Par->dt / tfcas);
    NextSt->jca = fcass - (fcass - CurrentSt->jca) * exp(-Par->dt / tjca);
    NextSt->ffp = Var->fss - (Var->fss - CurrentSt->ffp) * exp(-Par->dt / tffp);
    NextSt->fcafp = fcass - (fcass - CurrentSt->fcafp) * exp(-Par->dt / tfcafp);
    NextSt->nca = anca * k2n / km2n - (anca * k2n / km2n - CurrentSt->nca) * exp(-km2n * Par->dt);

    double PCa = 0.0001 * Cnst->c_pca;
    if (Cnst->celltype == 1) {
        PCa *= 1.2;
    }
    if (Cnst->celltype == 2) {
        PCa *= 2.5;
    }
    double PCap = 1.1 * PCa;
    double PCaNa = 0.00125 * PCa;
    double PCaK = 3.574e-4 * PCa;
    double PCaNap = 0.00125 * PCap;
    double PCaKp = 3.574e-4 * PCap;
    double fICaLp = (1.0 / (1.0 + Cnst->KmCaMK / Var->CaMKa));

    Var->ICaL = (1.0 - fICaLp) * PCa * PhiCaL * CurrentSt->d *
                (f * (1.0 - CurrentSt->nca) + CurrentSt->jca * fca * CurrentSt->nca) +
                fICaLp * PCap * PhiCaL * CurrentSt->d *
                (fp * (1.0 - CurrentSt->nca) + CurrentSt->jca * fcap * CurrentSt->nca);
    Var->ICaNa = (1.0 - fICaLp) * PCaNa * PhiCaNa * CurrentSt->d *
                 (f * (1.0 - CurrentSt->nca) + CurrentSt->jca * fca * CurrentSt->nca) +
                 fICaLp * PCaNap * PhiCaNa * CurrentSt->d *
                 (fp * (1.0 - CurrentSt->nca) + CurrentSt->jca * fcap * CurrentSt->nca);
    Var->ICaK = (1.0 - fICaLp) * PCaK * PhiCaK * CurrentSt->d *
                (f * (1.0 - CurrentSt->nca) + CurrentSt->jca * fca * CurrentSt->nca) +
                fICaLp * PCaKp * PhiCaK * CurrentSt->d *
                (fp * (1.0 - CurrentSt->nca) + CurrentSt->jca * fcap * CurrentSt->nca);
}