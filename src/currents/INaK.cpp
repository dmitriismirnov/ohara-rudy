//
// Created by andrey on 06/12/18.
//

#include "INaK.h"

void update_INaK(State *St, Variables *Var, Constants *Cnst) {

    double k1p = 949.5;
    double k1m = 182.4;
    double k2p = 687.2;
    double k2m = 39.4;
    double k3p = 1899.0;
    double k3m = 79300.0;
    double k4p = 639.0;
    double k4m = 40.0;
    double Knai0 = 9.073;
    double Knao0 = 27.78;

    double delta = -0.1550;
    double Knai = Knai0 * exp((delta * St->v * F) / (3.0 * R * T));
    if (Cnst->ISO == true) {
        Knai *= 0.72 * Cnst->c_Knai;
    }
    double Knao = Knao0 * exp(((1.0 - delta) * St->v * F) / (3.0 * R * T));
    double Kki = 0.5;
    double Kko = 0.3582;
    double MgADP = 0.05;
    double MgATP = 9.8;
    double Kmgatp = 1.698e-7;
    double H = 1.0e-7;
    double eP = 4.2;
    double Khp = 1.698e-7;
    double Knap = 224.0;
    double Kxkur = 292.0;
    double P = eP / (1.0 + H / Khp + St->nai / Knap + St->ki / Kxkur);
    double a1 = (k1p * pow(St->nai / Knai, 3.0)) /
                (pow(1.0 + St->nai / Knai, 3.0) + pow(1.0 + St->ki / Kki, 2.0) - 1.0);
    double b1 = k1m * MgADP;
    double a2 = k2p;
    double b2 = (k2m * pow(Cnst->nao / Knao, 3.0)) /
                (pow(1.0 + Cnst->nao / Knao, 3.0) + pow(1.0 + Cnst->ko / Kko, 2.0) - 1.0);
    double a3 = (k3p * pow(Cnst->ko / Kko, 2.0)) /
                (pow(1.0 + Cnst->nao / Knao, 3.0) + pow(1.0 + Cnst->ko / Kko, 2.0) - 1.0);
    double b3 = (k3m * P * H) / (1.0 + MgATP / Kmgatp);
    double a4 = (k4p * MgATP / Kmgatp) / (1.0 + MgATP / Kmgatp);
    double b4 = (k4m * pow(St->ki / Kki, 2.0)) /
                (pow(1.0 + St->nai / Knai, 3.0) + pow(1.0 + St->ki / Kki, 2.0) - 1.0);

    double x1 = a4 * a1 * a2 + b2 * b4 * b3 + a2 * b4 * b3 + b3 * a1 * a2;
    double x2 = b2 * b1 * b4 + a1 * a2 * a3 + a3 * b1 * b4 + a2 * a3 * b4;
    double x3 = a2 * a3 * a4 + b3 * b2 * b1 + b2 * b1 * a4 + a3 * a4 * b1;
    double x4 = b4 * b3 * b2 + a3 * a4 * a1 + b2 * a4 * a1 + b3 * b2 * a1;
    double E1 = x1 / (x1 + x2 + x3 + x4);
    double E2 = x2 / (x1 + x2 + x3 + x4);
    double E3 = x3 / (x1 + x2 + x3 + x4);
    double E4 = x4 / (x1 + x2 + x3 + x4);

    double zk = 1.0;
    double JnakNa = 3.0 * (E1 * a3 - E2 * b3);
    double JnakK = 2.0 * (E4 * b1 - E3 * a1);
    double Pnak = 30 * Cnst->c_pnak;
    if (Cnst->celltype == 1) {
        Pnak *= 0.9;
    }
    if (Cnst->celltype == 2) {
        Pnak *= 0.7;
    }
    double zna = 1.0;
    Var->INaK = Pnak * (zna * JnakNa + zk * JnakK);
}