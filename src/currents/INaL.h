//
// Created by andrey on 06/12/18.
//

#ifndef INTEGRATION_INAL_H
#define INTEGRATION_INAL_H

#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Parameters.h"
#include "../Constants.h"
#include "../Tables.h"
#include "subroutine.h"

void update_INaL(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                 Table *Tbl = nullptr);

#endif //INTEGRATION_INAL_H
