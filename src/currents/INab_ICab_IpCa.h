//
// Created by andrey on 06/12/18.
//

#ifndef INTEGRATION_INAB_ICAB_IPCA_H
#define INTEGRATION_INAB_ICAB_IPCA_H

#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Constants.h"

void update_INab_ICab_IpCa(State *St, Variables *Var, Constants *Cnst);

#endif //INTEGRATION_INAB_ICAB_IPCA_H
