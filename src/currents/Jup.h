//
// Created by andrey on 06/12/18.
//

#ifndef INTEGRATION_JUP_H
#define INTEGRATION_JUP_H

#include "../State.h"
#include "../Variables.h"
#include "../Constants.h"

void update_Jup(State *St, Variables *Var, Constants *Cnst);

#endif //INTEGRATION_JUP_H
