//
// Created by andrey on 05/12/18.
//

#include "Ito.h"

void update_Ito(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                Table *Tbl) {

    double ta, ass, tiF, tiS, iss, assp, tiFp, tiSp;

    if (Tbl != nullptr) {

        int ip; // integer part
        double fp; // fractional part
        Tbl->get_int_frac(CurrentSt->v, ip, fp);
        INTERPOLATE(Tbl, ass, ip, fp);
        INTERPOLATE(Tbl, ta, ip, fp);
        INTERPOLATE(Tbl, iss, ip, fp);
        INTERPOLATE(Tbl, tiF, ip, fp);
        INTERPOLATE(Tbl, tiS, ip, fp);
        INTERPOLATE(Tbl, assp, ip, fp);
        INTERPOLATE(Tbl, tiFp, ip, fp);
        INTERPOLATE(Tbl, tiSp, ip, fp);

    } else {
        ass = calculate_ass(CurrentSt->v);
        ta = calculate_ta(CurrentSt->v);

        iss = calculate_iss(CurrentSt->v);

        tiF = calculate_tiF(CurrentSt->v, Cnst->celltype);
        tiS = calculate_tiS(CurrentSt->v, Cnst->celltype);

        assp = calculate_assp(CurrentSt->v);

        tiFp = calculate_tiFp(CurrentSt->v, tiF);
        tiSp = calculate_tiSp(CurrentSt->v, tiS);
    }

    double AiF = 1.0 / (1.0 + exp((CurrentSt->v - 213.6) / 151.2));
    double AiS = 1.0 - AiF;
    double i = AiF * CurrentSt->iF + AiS * CurrentSt->iS;
    double ip = AiF * CurrentSt->iFp + AiS * CurrentSt->iSp;

    NextSt->a = ass - (ass - CurrentSt->a) * exp(-Par->dt / ta);
    NextSt->iF = iss - (iss - CurrentSt->iF) * exp(-Par->dt / tiF);
    NextSt->iS = iss - (iss - CurrentSt->iS) * exp(-Par->dt / tiS);
    NextSt->ap = assp - (assp - CurrentSt->ap) * exp(-Par->dt / ta);
    NextSt->iFp = iss - (iss - CurrentSt->iFp) * exp(-Par->dt / tiFp);
    NextSt->iSp = iss - (iss - CurrentSt->iSp) * exp(-Par->dt / tiSp);

    double Gto = 0.02 * Cnst->c_gto;
    if (Cnst->celltype == 1) {
        Gto *= 4.0;
    }
    if (Cnst->celltype == 2) {
        Gto *= 4.0;
    }

    double fItop = (1.0 / (1.0 + Cnst->KmCaMK / Var->CaMKa));
    Var->Ito = Gto * (CurrentSt->v - Var->EK) *
               ((1.0 - fItop) * CurrentSt->a * i + fItop * CurrentSt->ap * ip);
}
