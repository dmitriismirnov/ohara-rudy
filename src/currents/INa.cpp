//
// Created by andrey on 05/12/18.
//

#include "INa.h"


void update_INa(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                Table *Tbl) {

    Var->CaMKb = Cnst->CaMKo * Cnst->c_CaMKo * (1.0 - CurrentSt->CaMKt) / (1.0 + Cnst->KmCaM / CurrentSt->cass);

    Var->CaMKa = Var->CaMKb + CurrentSt->CaMKt;

    double mss, hss, hssp, tm, thf, ths, tj;

    if (Tbl != nullptr) {
        int ip; // integer part
        double fp; // fractional part
        Tbl->get_int_frac(CurrentSt->v, ip, fp);
        INTERPOLATE(Tbl, mss, ip, fp);
        INTERPOLATE(Tbl, hss, ip, fp);
        INTERPOLATE(Tbl, hssp, ip, fp);
        INTERPOLATE(Tbl, tm, ip, fp);
        INTERPOLATE(Tbl, thf, ip, fp);
        INTERPOLATE(Tbl, ths, ip, fp);
        INTERPOLATE(Tbl, tj, ip, fp);
    } else {

        if (Cnst->ISO == true) {
            mss = calculate_mss(CurrentSt->v, Cnst->c_INa_dV_act);
            hss = calculate_hss(CurrentSt->v, Cnst->c_INa_dV_inact);
            hssp = calculate_hssp(CurrentSt->v, Cnst->c_INa_dV_inact);
        } else {
            mss = calculate_mss(CurrentSt->v);
            hss = calculate_hss(CurrentSt->v);
            hssp = calculate_hssp(CurrentSt->v);
        }

        tm = calculate_tm(CurrentSt->v);
        thf = calculate_thf(CurrentSt->v);
        ths = calculate_ths(CurrentSt->v);
        tj = calculate_tj(CurrentSt->v);
    }

    double Ahf = 0.99;
    double Ahs = 1.0 - Ahf;
    double jss = hss;
    double thsp = 3.0 * ths;
    double tjp = 1.46 * tj;

    NextSt->m = mss - (mss - CurrentSt->m) * exp(-Par->dt / tm);
    NextSt->hf = hss - (hss - CurrentSt->hf) * exp(-Par->dt / thf);
    NextSt->hs = hss - (hss - CurrentSt->hs) * exp(-Par->dt / ths);
    NextSt->j = jss - (jss - CurrentSt->j) * exp(-Par->dt / tj);
    NextSt->hsp = hssp - (hssp - CurrentSt->hsp) * exp(-Par->dt / thsp);
    NextSt->jp = jss - (jss - CurrentSt->jp) * exp(-Par->dt / tjp);

    double h = Ahf * CurrentSt->hf + Ahs * CurrentSt->hs;
    double hp = Ahf * CurrentSt->hf + Ahs * CurrentSt->hsp;

    double GNa = 75.;
    if (Cnst->ISO == true) {
        GNa *= 2.35 * Cnst->c_GNa_ISO;
    } else {
        GNa *= Cnst->c_gnal;
    }
    double fINap = (1.0 / (1.0 + Cnst->KmCaMK / Var->CaMKa));

    Var->INa = GNa * (CurrentSt->v - Var->ENa) * CurrentSt->m * CurrentSt->m * CurrentSt->m *
               ((1.0 - fINap) * h * CurrentSt->j + fINap * hp * CurrentSt->jp);
}