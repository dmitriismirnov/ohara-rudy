//
// Created by andrey on 05/12/18.
//

#ifndef INTEGRATION_IK1_H
#define INTEGRATION_IK1_H

#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Parameters.h"
#include "../Constants.h"
#include "../Tables.h"
#include "subroutine.h"

void update_IK1(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                Table *Tbl = nullptr);

#endif //INTEGRATION_IK1_H
