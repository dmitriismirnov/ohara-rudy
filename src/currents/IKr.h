//
// Created by andrey on 05/12/18.
//

#ifndef INTEGRATION_IKR_H
#define INTEGRATION_IKR_H

#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Parameters.h"
#include "../Constants.h"
#include "../Tables.h"
#include "subroutine.h"

void update_IKr(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                Table *Tbl = nullptr);

#endif //INTEGRATION_IKR_H
