//
// Created by andrey on 05/12/18.
//

#include "IKr.h"

void update_IKr(State *CurrentSt, State *NextSt,
                Variables *Var, Parameters *Par, Constants *Cnst,
                Table *Tbl) {

    double GKr = 0.046 * Cnst->c_gkr;

    if (Cnst->IKR_GRANDI == true) {

        double xrss, txrs, rkr;
        if (Tbl != nullptr) {
            int ip; // integer part
            double fp; // fractional part
            Tbl->get_int_frac(CurrentSt->v, ip, fp);
            INTERPOLATE(Tbl, xrss, ip, fp);
            INTERPOLATE(Tbl, txrs, ip, fp);
            INTERPOLATE(Tbl, rkr, ip, fp);
        } else {
            xrss = calculate_xrss(CurrentSt->v, Cnst->IKR_GRANDI);
            txrs = calculate_txrs(CurrentSt->v, Cnst->IKR_GRANDI);
            rkr = calculate_rkr(CurrentSt->v, Cnst->IKR_GRANDI);
        }

        NextSt->xrs = xrss - (xrss - CurrentSt->xrs) * exp(-Par->dt / txrs);

        if (Cnst->celltype == 1)
        {
            GKr *= 1.3;
        }
        if (Cnst->celltype == 2)
        {
            GKr *= 0.8;
        }
        Var->IKr = GKr * sqrt(Cnst->ko / 5.4) * CurrentSt->xrs * rkr * (CurrentSt->v - Var->EK);

    } else { // Cnst->IKR_GRANDI == false

        double xrss, txrs, rkr, txrf, Axrf;
        if (Tbl != nullptr) {
            int ip; // integer part
            double fp; // fractional part
            Tbl->get_int_frac(CurrentSt->v, ip, fp);
            INTERPOLATE(Tbl, xrss, ip, fp);
            INTERPOLATE(Tbl, txrs, ip, fp);
            INTERPOLATE(Tbl, rkr, ip, fp);
            INTERPOLATE(Tbl, txrf, ip, fp);
            INTERPOLATE(Tbl, Axrf, ip, fp);
        } else {
            xrss = calculate_xrss(CurrentSt->v, Cnst->IKR_GRANDI);
            txrs = calculate_txrs(CurrentSt->v, Cnst->IKR_GRANDI);
            rkr = calculate_rkr(CurrentSt->v, Cnst->IKR_GRANDI);
            txrf = calculate_txrf(CurrentSt->v);
            Axrf = calculate_Axrf(CurrentSt->v);
        }

        double Axrs = 1.0 - Axrf;
        NextSt->xrf = xrss - (xrss - CurrentSt->xrf) * exp(-Par->dt / txrf);
        NextSt->xrs = xrss - (xrss - CurrentSt->xrs) * exp(-Par->dt / txrs);
        double xr = Axrf * CurrentSt->xrf + Axrs * CurrentSt->xrs;

        if (Cnst->celltype == 1)
        {
            GKr *= 1.3;
        }
        if (Cnst->celltype == 2)
        {
            GKr *= 0.8;
        }
        Var->IKr = GKr * sqrt(Cnst->ko / 5.4) * xr * rkr * (CurrentSt->v - Var->EK);
    }
}