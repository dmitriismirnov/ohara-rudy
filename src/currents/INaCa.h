//
// Created by andrey on 06/12/18.
//

#ifndef INTEGRATION_INACA_H
#define INTEGRATION_INACA_H

#include <cmath>

#include "../State.h"
#include "../Variables.h"
#include "../Constants.h"

void update_INaCa(State *St, Variables *Var, Constants *Cnst);

#endif //INTEGRATION_INACA_H
