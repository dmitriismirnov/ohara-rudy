//
// Created by andrey on 05/12/18.
//

#include "Jrel.h"

void update_Jrel(State *CurrentSt, State *NextSt,
                 Variables *Var, Parameters *Par, Constants *Cnst) {
    double bt = 4.75;
    double a_rel = 0.5 * bt;
    if (Cnst->ISO == true) {
        a_rel *= 1.75 * Cnst->c_a_rel;
    }
    double Jrel_inf = Cnst->c_jrelinf * a_rel * Cnst->c_arel * (-Var->ICaL) / (1.0 + pow(1.5 / CurrentSt->cajsr, 8.0));
    if (Cnst->celltype == 2) {
        Jrel_inf *= 1.7;
    }
    double tau_rel = bt / (1.0 + 0.0123 / CurrentSt->cajsr);
    if (Cnst->ISO == true) {
        tau_rel *= 0.5 * Cnst->c_tau_rel;
    }

    if (tau_rel < 0.005) {
        tau_rel = 0.005;
    }

    NextSt->Jrelnp = Jrel_inf - (Jrel_inf - CurrentSt->Jrelnp) * exp(-Par->dt / tau_rel);

    double btp = 1.25 * bt;
    double a_relp = 0.5 * btp;
    if (Cnst->ISO == true) {
        a_relp *= 1.75 * Cnst->c_a_rel;
    }
    double Jrel_infp =
            Cnst->c_jrelinf * Cnst->c_arel * a_relp * (-Var->ICaL) / (1.0 + pow(1.5 / CurrentSt->cajsr, 8.0));
    if (Cnst->celltype == 2) {
        Jrel_infp *= 1.7;
    }
    double tau_relp = btp / (1.0 + 0.0123 / CurrentSt->cajsr);
    if (Cnst->ISO == true) {
        tau_relp *= 0.5 * Cnst->c_tau_rel;
    }

    if (tau_relp < 0.005) {
        tau_relp = 0.005;
    }


    NextSt->Jrelp = Jrel_infp - (Jrel_infp - CurrentSt->Jrelp) * exp(-Par->dt / tau_relp);
    double fJrelp = (1.0 / (1.0 + Cnst->KmCaMK / Var->CaMKa));
    Var->Jrel = (1.0 - fJrelp) * CurrentSt->Jrelnp + fJrelp * CurrentSt->Jrelp;
}
