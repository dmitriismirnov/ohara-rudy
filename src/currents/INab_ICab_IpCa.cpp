//
// Created by andrey on 06/12/18.
//

#include "INab_ICab_IpCa.h"

void update_INab_ICab_IpCa(State *St, Variables *Var, Constants *Cnst) {

    double xkb = 1.0 / (1.0 + exp(-(St->v - 14.48) / 18.34));
    double GKb = 0.003;
    if (Cnst->ISO == true) {
        GKb *= 3 * Cnst->c_GKb_iso;
    } else {
        GKb *= Cnst->c_gkb;
    }
    if (Cnst->celltype == 1) {
        GKb *= 0.6;
    }
    Var->IKb = GKb * xkb * (St->v - Var->EK);

    double PNab = 3.75e-10;
    Var->INab = Cnst->c_gnab * PNab * (St->v * FFRT) * (St->nai * exp((St->v * FRT)) - Cnst->nao) /
                (exp((St->v * FRT)) - 1.0);

    double PCab = 2.5e-8;
    Var->ICab = Cnst->c_gcab * PCab * 4.0 * (St->v * FFRT) * (St->cai * exp(2.0 * (St->v * FRT)) - 0.341 * Cnst->cao) /
                (exp(2.0 * (St->v * FRT)) - 1.0);

    double GpCa = 0.0005 * Cnst->c_gpca;
    Var->IpCa = GpCa * St->cai / (0.0005 + St->cai);
}