//
// Created by andrey on 03/12/18.
//

#include "RGC.h"


void RGC(State *CurrentState, State *NextState,
         Variables *Var, Parameters *Par, Constants *Cnst,
         Table *Tbl) {

    for (int i = 0; i < Par->chain_length; i++) {

        if ((Tbl != nullptr) && ((CurrentState[i].v <= Tbl->v_min) || (CurrentState[i].v >= Tbl->v_max))) {
            std::cerr << "V is not in Table range!\n";
            std::cerr << "V = " << CurrentState[i].v << ", Table = [" << Tbl->v_min << ", " << Tbl->v_max << "]\n";
            abort();
        }

        ///* 0. Calculate reversal potentials*/
        Var[i].ENa = RTF * log(Cnst->nao / CurrentState[i].nai);
        Var[i].EK = RTF * log(Cnst->ko / CurrentState[i].ki);
        Var[i].EKs = RTF * log((Cnst->ko + 0.01833 * Cnst->nao) / (CurrentState[i].ki + 0.01833 * CurrentState[i].nai));

        ///update_SOME_CURRENT(&CurrentState[i], &NextState[i], Var, Par, Cnst) will update CurrentState and Var !!!

        ///* 1. Calculate INa_fast*/
        update_INa(&CurrentState[i], &NextState[i], &Var[i], Par, Cnst, Tbl);

        ///* 2. Calculate INa_late*/
        update_INaL(&CurrentState[i], &NextState[i], &Var[i], Par, Cnst, Tbl);

        ///* 3. Calculate Ito*/
        update_Ito(&CurrentState[i], &NextState[i], &Var[i], Par, Cnst, Tbl);

        ///* 4-6. Calculate ICaL, ICaNa, ICaK */
        update_ICaL_ICaN_ICaK(&CurrentState[i], &NextState[i], &Var[i], Par, Cnst, Tbl);

        ///* 7. Calculate IKr */
        update_IKr(&CurrentState[i], &NextState[i], &Var[i], Par, Cnst, Tbl);

        ///* 8. Calculate IKs */
        update_IKs(&CurrentState[i], &NextState[i], &Var[i], Par, Cnst, Tbl);

        ///* 9. Calculate IK1 */
        update_IK1(&CurrentState[i], &NextState[i], &Var[i], Par, Cnst, Tbl);

        ///* 10. Calculate INaCa */
        update_INaCa(&CurrentState[i], &Var[i], Cnst);

        ///* 11. Calculate INaK*/
        update_INaK(&CurrentState[i], &Var[i], Cnst);

        ///* 12-14. Calculate INab, ICab, IpCa */
        update_INab_ICab_IpCa(&CurrentState[i], &Var[i], Cnst);

        ///* 15. Calculate IKATP */
        update_IKATP(&CurrentState[i], &Var[i], Cnst);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ///* 16. Calculate Ist for the first cell */
        Var[i].Ist = 0.0;
        if (i == 0){
            if ((Par->t >= Par->t_stimulus) && (Par->t < Par->t_stimulus + Par->duration)) {
                Var[i].Ist = Par->amplitude;
                Par->update_beats_count();
            } else if (Par->t >= Par->t_stimulus + Par->duration) {
                Var[i].Ist = 0.0;
                Par->update_stimulus();
            }
        }


        ///* 17. Calculate I_gap_junc */
        Var[i].I_gap_junc = 0;

        if (Par->chain_length > 1) {

            if (i < (Par->chain_length - 1)) {
                Var[i].I_gap_junc += -Cnst->g_gap_junc * (CurrentState[i + 1].v - CurrentState[i].v);
            }
            if (i > 0) {
                Var[i].I_gap_junc += -Cnst->g_gap_junc * (-CurrentState[i].v + CurrentState[i - 1].v);
            }
        }


        ///* Calculate Voltage */
        NextState[i].v = CurrentState[i].v - Par->dt *
                                            (Var[i].INa + Var[i].INaL + Var[i].Ito + Var[i].ICaL + Var[i].ICaNa +
                                            Var[i].ICaK + Var[i].IKr + Var[i].IKs + Var[i].IK1 + Var[i].INaCa +
                                            Var[i].INaK + Var[i].INab + Var[i].IKb + Var[i].IpCa + Var[i].ICab +
                                            Var[i].Ist + Var[i].IKatp + Var[i].I_gap_junc);


        ///------------old: FBS------------//

        Var[i].CaMKb = Cnst->CaMKo * Cnst->c_CaMKo * (1.0 - CurrentState[i].CaMKt) /
                     (1.0 + Cnst->KmCaM / CurrentState[i].cass);

        Var[i].CaMKa = Var[i].CaMKb + CurrentState[i].CaMKt;

        NextState[i].CaMKt = CurrentState[i].CaMKt + Par->dt *
                                                     (Cnst->aCaMK * Var[i].CaMKb * (Var[i].CaMKb + CurrentState[i].CaMKt) -
                                                      Cnst->bCaMK * CurrentState[i].CaMKt);

        Var[i].JdiffNa = (CurrentState[i].nass - CurrentState[i].nai) / 2.0;

        Var[i].JdiffK = (CurrentState[i].kss - CurrentState[i].ki) / 2.0;

        Var[i].Jdiff = (CurrentState[i].cass - CurrentState[i].cai) / 0.2;

        update_Jrel(&CurrentState[i], &NextState[i], &Var[i], Par, Cnst);

        /////////////////
        Var[i].Jleak = 0.0039375 * CurrentState[i].cansr / 15.0; /// MUST BE BEFORE JUP
        update_Jup(&CurrentState[i], &Var[i], Cnst); /// DEPENDS ON J_LEAK!!!!!!
        /////////////////

        Var[i].Jtr = (CurrentState[i].cansr - CurrentState[i].cajsr) / 100.0;

        NextState[i].nai = CurrentState[i].nai + Par->dt *
                                                 (-(Var[i].INa + Var[i].INaL + 3.0 * Var[i].INaCa_i + 3.0 * Var[i].INaK +
                                                    Var[i].INab) * Cnst->Acap / (F * Cnst->vmyo) +
                                                  Var[i].JdiffNa * Cnst->vss / Cnst->vmyo);

        NextState[i].nass = CurrentState[i].nass + Par->dt *
                                                   (-(Var[i].ICaNa + 3.0 * Var[i].INaCa_ss) * Cnst->Acap / (F * Cnst->vss) -
                                                    Var[i].JdiffNa);

        NextState[i].ki = CurrentState[i].ki + Par->dt *
                                               (-(Var[i].Ito + Var[i].IKr + Var[i].IKs + Var[i].IK1 + Var[i].IKb + Var[i].IKatp +
                                                  Var[i].Ist - 2.0 * Var[i].INaK) * Cnst->Acap / (F * Cnst->vmyo) +
                                                Var[i].JdiffK * Cnst->vss / Cnst->vmyo);

        NextState[i].kss = CurrentState[i].kss + Par->dt * (-(Var[i].ICaK) * Cnst->Acap / (F * Cnst->vss) - Var[i].JdiffK);

        double Bcai;

        if (Cnst->ISO) {
            Bcai = 1.0 /
                   (1.0 + Cnst->c_CMDN * Cnst->cmdnmax * Cnst->kmcmdn / pow(Cnst->kmcmdn + CurrentState[i].cai, 2.0) +
                    Cnst->trpnmax * Cnst->kmtrpn * 1.6 * Cnst->c_Tnl /
                    pow(Cnst->kmtrpn * 1.6 * Cnst->c_Tnl + CurrentState[i].cai, 2.0));
        } else {
            Bcai = 1.0 /
                   (1.0 + Cnst->c_CMDN * Cnst->cmdnmax * Cnst->kmcmdn / pow(Cnst->kmcmdn + CurrentState[i].cai, 2.0) +
                    Cnst->trpnmax * Cnst->kmtrpn / pow(Cnst->kmtrpn + CurrentState[i].cai, 2.0));
        }

        NextState[i].cai = CurrentState[i].cai + Par->dt * (Bcai * (-(Var[i].IpCa + Var[i].ICab - 2.0 * Var[i].INaCa_i) *
                                                                    Cnst->Acap / (2.0 * F * Cnst->vmyo) -
                                                                    Var[i].Jup * Cnst->vnsr / Cnst->vmyo +
                                                                    Var[i].Jdiff * Cnst->vss / Cnst->vmyo));

        double Bcass = 1.0 / (1.0 + Cnst->BSRmax * Cnst->KmBSR / pow(Cnst->KmBSR + CurrentState[i].cass, 2.0) +
                              Cnst->BSLmax * Cnst->KmBSL / pow(Cnst->KmBSL + CurrentState[i].cass, 2.0));

        NextState[i].cass = CurrentState[i].cass + Par->dt * (Bcass * (-(Var[i].ICaL - 2.0 * Var[i].INaCa_ss) * Cnst->Acap /
                                                                       (2.0 * F * Cnst->vss) +
                                                                       Var[i].Jrel * Cnst->vjsr / Cnst->vss -
                                                                       Var[i].Jdiff));

        NextState[i].cansr = CurrentState[i].cansr + Par->dt * (Var[i].Jup - Var[i].Jtr * Cnst->vjsr / Cnst->vnsr);

        double Bcajsr = 1.0 / (1.0 + Cnst->csqnmax * Cnst->kmcsqn / pow(Cnst->kmcsqn + CurrentState[i].cajsr, 2.0));

        NextState[i].cajsr = CurrentState[i].cajsr + Par->dt * (Bcajsr * (Var[i].Jtr - Var[i].Jrel));
    }

}
