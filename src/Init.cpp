//
// Created by andrey on 02/12/18.
//

#include "Init.h"

Constants *load_constants_from_file(std::string const &filename) {

    ConfigFile cf(filename);

    /// section [conductances]
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gkatp);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_jrelinf);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gk1);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gkr);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gks);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gnal);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gto);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_pca);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gncx);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_pnak);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gpca);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_jup);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_CMDN);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_arel);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_CaMKo);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gkb);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gcab);
    DEFINE_FROM_CONFIGFILE(double, cf, conductances, c_gnab);

    /// section [iso]
    DEFINE_FROM_CONFIGFILE(bool, cf, iso, ISO);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_INa_dV_act);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_INa_dV_inact);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_ICaL_dV_act);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_ICaL_dV_inact);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_Knai);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_GKb_iso);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_SERCA);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_GNa_ISO);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_Tnl);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_IKs_ISO);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_txs_1_iso);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_a_rel);
    DEFINE_FROM_CONFIGFILE(double, cf, iso, c_tau_rel);

    /// section [concentrations]
    DEFINE_FROM_CONFIGFILE(double, cf, concentrations, nao);
    DEFINE_FROM_CONFIGFILE(double, cf, concentrations, cao);
    DEFINE_FROM_CONFIGFILE(double, cf, concentrations, ko);

    /// section [cell_properties]
    DEFINE_FROM_CONFIGFILE(int, cf, cell_properties, celltype);

    /// section [gap]
    DEFINE_FROM_CONFIGFILE(double, cf, gap, g_gap_junc);

    /// section [ikatp]
    DEFINE_FROM_CONFIGFILE(double, cf, ikatp, f_atp);

    ///section [flags]
    DEFINE_FROM_CONFIGFILE(bool, cf, flags, IKR_GRANDI);

    Constants *C = new Constants(c_gkatp, c_jrelinf, c_gk1, c_gkr, c_gks, c_gnal,
                                 c_gto, c_pca, c_gncx, c_pnak, c_gpca, c_jup, c_CMDN,
                                 c_arel, c_CaMKo, c_gkb, c_gcab, c_gnab,

                                 ISO,
                                 c_INa_dV_act, c_INa_dV_inact, c_ICaL_dV_act,
                                 c_ICaL_dV_inact, c_Knai, c_GKb_iso, c_SERCA,
                                 c_GNa_ISO, c_Tnl, c_IKs_ISO, c_txs_1_iso,
                                 c_a_rel, c_tau_rel,

                                 nao, cao, ko,

                                 celltype,

                                 g_gap_junc,

                                 f_atp,

                                 IKR_GRANDI);

    return C;
}

Parameters *load_parameters_from_file(std::string const &filename) {

    ConfigFile cf(filename);

    /// section [times]
    DEFINE_FROM_CONFIGFILE(double, cf, times, dt);
    DEFINE_FROM_CONFIGFILE(double, cf, times, safe_time);

    /// section [output]
    DEFINE_FROM_CONFIGFILE(double, cf, output, output_step);
    DEFINE_FROM_CONFIGFILE(int, cf, output, beats_save);

    /// section [chain]
    DEFINE_FROM_CONFIGFILE(int, cf, chain, chain_length);

    /// section [stimulation]
    DEFINE_FROM_CONFIGFILE(double, cf, stimulation, amplitude);
    DEFINE_FROM_CONFIGFILE(double, cf, stimulation, start);
    DEFINE_FROM_CONFIGFILE(double, cf, stimulation, duration);
    DEFINE_FROM_CONFIGFILE(int, cf, stimulation, number_of_beats);
    DEFINE_FROM_CONFIGFILE(double, cf, stimulation, CL_initial);

    DEFINE_FROM_CONFIGFILE(bool, cf, stimulation, DECREASE_CL);
    DEFINE_FROM_CONFIGFILE(double, cf, stimulation, CL_final);
    DEFINE_FROM_CONFIGFILE(double, cf, stimulation, bpm_step);
    DEFINE_FROM_CONFIGFILE(int, cf, stimulation, beats_final);

    Parameters *P = new Parameters(dt, safe_time,
                                   output_step, beats_save,
                                   chain_length,
                                   amplitude, start, duration, number_of_beats,
                                   CL_initial,
                                   DECREASE_CL, CL_final, bpm_step,
                                   beats_final);

    return P;
}

//initial values for state variables, there are 41 of them
void initialize_state(State *St) {

    St->v         = -87.5;
    St->nai       = 7.;
    St->nass      = 7.;
    St->ki        = 145.;
    St->kss       = 145.;
    St->cai       = 1.0e-4;
    St->cass      = 1.0e-4;
    St->cansr     = 1.2;
    St->cajsr     = 1.2;
    St->m         = 0;
    St->hf        = 1;
    St->hs        = 1;
    St->j         = 1;
    St->hsp       = 1;
    St->jp        = 1;
    St->mL        = 0;
    St->hL        = 1;
    St->hLp       = 1;
    St->a         = 0;
    St->iF        = 1;
    St->iS        = 1;
    St->ap        = 0;
    St->iFp       = 1;
    St->iSp       = 1;
    St->d         = 0;
    St->ff        = 1;
    St->fs        = 1;
    St->fcaf      = 1;
    St->fcas      = 1;
    St->jca       = 1;
    St->nca       = 0;
    St->ffp       = 1;
    St->fcafp     = 1;
    St->xrf       = 0;
    St->xrs       = 0;
    St->xs1       = 0;
    St->xs2       = 0;
    St->xk1       = 1;
    St->Jrelnp    = 0;
    St->Jrelp     = 0;
    St->CaMKt     = 0;

}

void update_state_from_file(State *St, std::string filename) {

    FILE *fin = fopen(filename.c_str(), "r");
    if (!fin) {
        std::string report = "Can't open \"" + filename + "\" for state initialization";
        throw std::runtime_error(report);
    } else {
        fread(St, sizeof(State), 1, fin);
    }
    fclose(fin);

}