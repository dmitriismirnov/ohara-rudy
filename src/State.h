//
// Created by andrey on 03/12/18.
//

#ifndef OHARA_RUDY_STATE_H
#define OHARA_RUDY_STATE_H

struct State {
    double v;
    double nai;
    double nass;
    double ki;
    double kss;
    double cai;
    double cass;
    double cansr;
    double cajsr;
    double m;
    double hf;
    double hs;
    double j;
    double hsp;
    double jp;
    double mL;
    double hL;
    double hLp;
    double a;
    double iF;
    double iS;
    double ap;
    double iFp;
    double iSp;
    double d;
    double ff;
    double fs;
    double fcaf;
    double fcas;
    double jca;
    double nca;
    double ffp;
    double fcafp;
    double xrf;
    double xrs;
    double xs1;
    double xs2;
    double xk1;
    double Jrelnp;
    double Jrelp;
    double CaMKt;
};

#endif //OHARA_RUDY_STATE_H
