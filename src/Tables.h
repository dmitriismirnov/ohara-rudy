//
// Created by andrey on 03/12/18.
//

#ifndef OHARA_RUDY_TABLES_H
#define OHARA_RUDY_TABLES_H

#include <cmath>

#include "Constants.h"
#include "currents/subroutine.h"

#define INTERPOLATE(table_name, item_name, ip, fp) \
(item_name = table_name->item_name[(ip)] + (fp) * (table_name->item_name[(ip) + 1] - table_name->item_name[(ip)]))

struct Table {

    explicit Table(double v_min = -100,
            double v_max = 100,
            int num = 1000,
            Constants *Cnst = nullptr) : v_min(v_min), v_max(v_max), num(num)

    {
        ///* INa */
        mss = new double[num];
        hss = new double[num];
        hssp = new double[num];
        tm = new double[num];
        thf = new double[num];
        ths = new double[num];
        tj = new double[num];

        for (int i = 0; i < num; ++i) {
            double v = v_min + (v_max - v_min) * i / (num - 1);
            if ((Cnst != nullptr) && (Cnst->ISO == true)) {
                mss[i] = calculate_mss(v, Cnst->c_INa_dV_act);
                hss[i] = calculate_hss(v, Cnst->c_INa_dV_inact);
                hssp[i] = calculate_hssp(v, Cnst->c_INa_dV_inact);
            } else {
                mss[i] = calculate_mss(v);
                hss[i] = calculate_hss(v);
                hssp[i] = calculate_hssp(v);
            }
            tm[i] = calculate_tm(v);
            thf[i] = calculate_thf(v);
            tj[i] = calculate_tj(v);
            ths[i] = calculate_ths(v);
        }
        
        ///* INaL */
        mLss = new double[num];
        hLss = new double[num];
        hLssp = new double[num];
        tmL = new double[num];

        for (int i = 0; i < num; ++i) {
            double v = v_min + (v_max - v_min) * i / (num - 1);
            mLss[i] = calculate_mLss(v);
            hLss[i] = calculate_hLss(v);
            hLssp[i] = calculate_hLssp(v);
            tmL[i] = calculate_tmL(v);
        }

        ///* Ito */
        ta = new double[num];
        ass = new double[num];
        tiF = new double[num];
        tiS = new double[num];
        iss = new double[num];
        assp = new double[num];
        tiFp = new double[num];
        tiSp = new double[num];

        for (int i = 0; i < num; ++i) {
            double v = v_min + (v_max - v_min) * i / (num - 1);
            ta[i] = calculate_ta(v);
            ass[i] = calculate_ass(v);
            tiF[i] = calculate_tiF(v, Cnst->celltype);
            tiS[i] = calculate_tiS(v, Cnst->celltype);
            iss[i] = calculate_iss(v);
            assp[i] = calculate_assp(v);
            tiFp[i] = calculate_tiFp(v, tiF[i]);
            tiSp[i] = calculate_tiSp(v, tiS[i]);
        }

        ///* ICaL, ICaNa, ICaK */
        td = new double[num];
        dss = new double[num];
        tff = new double[num];
        tfs = new double[num];
        fss = new double[num];
        tfcaf = new double[num];
        tfcas = new double[num];
        Afcaf = new double[num];

        for (int i = 0; i < num; ++i) {
            double v = v_min + (v_max - v_min) * i / (num - 1);
            if ((Cnst != nullptr) && (Cnst->ISO == true)) {
                dss[i] = calculate_dss(v, Cnst->c_ICaL_dV_act);
                fss[i] = calculate_fss(v, Cnst->c_ICaL_dV_inact);
            } else {
                dss[i] = calculate_dss(v);
                fss[i] = calculate_fss(v);
            }
            td[i] = calculate_td(v);
            tff[i] = calculate_tff(v);
            tfs[i] = calculate_tfs(v);
            tfcaf[i] = calculate_tfcaf(v);
            tfcas[i] = calculate_tfcas(v);
            Afcaf[i] = calculate_Afcaf(v);
        }

        ///* IKr */
        xrss = new double[num];
        txrs = new double[num];
        rkr = new double[num];
        txrf = new double[num];
        Axrf = new double[num];

        for (int i = 0; i < num; ++i) {
            double v = v_min + (v_max - v_min) * i / (num - 1);
            xrss[i] = calculate_xrss(v, Cnst->IKR_GRANDI);
            txrs[i] = calculate_txrs(v, Cnst->IKR_GRANDI);
            rkr[i] = calculate_rkr(v, Cnst->IKR_GRANDI);
            txrf[i] = calculate_txrf(v);
            Axrf[i] =  calculate_Axrf(v);
        }

        ///* IKs */
        xs1ss = new double[num];
        txs1 = new double[num];
        txs2 = new double[num];
        for (int i = 0; i < num; ++i) {
            double v = v_min + (v_max - v_min) * i / (num - 1);
            xs1ss[i] = calculate_xs1ss(v);
            txs1[i] = calculate_txs1(v);
            txs2[i] = calculate_txs2(v);
        }

        ///* IK1 */
        xk1ss = new double[num];
        txk1 = new double[num];
        rk1 = new double[num];
        for (int i = 0; i < num; ++i) {
            double v = v_min + (v_max - v_min) * i / (num - 1);
            xk1ss[i] = calculate_xk1ss(v, Cnst->ko);
            txk1[i] = calculate_txk1(v);
            rk1[i] = calculate_rk1(v, Cnst->ko);
        }
    }


    ~Table() {
        ///* INa */
        delete [] mss;
        delete [] hss;
        delete [] hssp;
        delete [] tm;
        delete [] thf;
        delete [] ths;
        delete [] tj;

        ///* INaL */
        delete [] mLss;
        delete [] hLss;
        delete [] hLssp;
        delete [] tmL;

        ///* Ito */
        delete [] ta;
        delete [] ass;
        delete [] tiF;
        delete [] tiS;
        delete [] iss;
        delete [] assp;
        delete [] tiFp;
        delete [] tiSp;

        ///* ICaL, ICaNa, ICaK */
        delete [] td;
        delete [] dss;
        delete [] tff;
        delete [] tfs;
        delete [] fss;
        delete [] tfcaf;
        delete [] tfcas;
        delete [] Afcaf;

        ///* IKr */
        delete [] xrss;
        delete [] txrs;
        delete [] rkr;
        delete [] txrf;
        delete [] Axrf;

        ///* IKs */
        delete [] xs1ss;
        delete [] txs1;
        delete [] txs2;

        ///* IK1 */
        delete [] xk1ss;
        delete [] txk1;
        delete [] rk1;
    }

    void get_int_frac(double v, int & intpart, double & fractpart) {
        double value = (v - v_min) / (v_max - v_min) * (num - 1);
        intpart = int (value);
        fractpart = value - intpart;
        return;
    }

    double v_min, v_max;
    int num;

    ///* INa */
    double *mss, *hss, *hssp, *tm, *thf, *ths, *tj;
    
    ///* INaL */
    double *mLss, *hLss, *hLssp, *tmL;

    ///* Ito */
    double *ta, *ass, *tiF, *tiS, *iss, *assp, *tiFp, *tiSp;

    ///* ICaL, ICaNa, ICaK */
    double *td, *dss, *tff, *tfs, *fss, *tfcaf, *tfcas, *Afcaf;

    ///* IKr */
    double *xrss, *txrs, *rkr, *txrf, *Axrf;

    ///* IKs */
    double *xs1ss, *txs1, *txs2;

    ///* IK1 */
    double *xk1ss, *txk1, *rk1;
};

#endif //OHARA_RUDY_TABLES_H
